pub mod artwork;
pub mod collection;
pub mod interface;

/// Plugin priorities
/// Max reserved, do not use
#[derive(Clone, PartialEq, Eq)]
pub enum PluginPriority {
    Web,
    Local,
    Max,
}
