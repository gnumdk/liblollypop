// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use super::PluginPriority;
use crate::providers::artwork::ArtworkInterface;

use std::boxed::Box;

/// An artwork plugin provides artwork to Lollypop
/// by implementing [`ArtworkInterface`](crate::providers::artwork::ArtworkInterface)
pub struct ArtworkPlugin {
    pub id: String,
    pub interface: Box<dyn ArtworkInterface + Send + Sync>,
    pub priority: PluginPriority,
}

impl ArtworkPlugin {
    /// Constructs a new ArtworkPlugin
    /// # Parameters
    ///
    /// * `id`: Uniq plugin id
    /// * `interface`: Artwork interface used by Lollypop to get artwork
    /// * `priority`: Plugin priority used by Lollypop to sort plugins
    pub fn new(
        id: String,
        interface: Box<dyn ArtworkInterface + Send + Sync>,
        priority: PluginPriority,
    ) -> ArtworkPlugin {
        let plugin = ArtworkPlugin {
            id,
            interface,
            priority,
        };
        plugin
    }
}
