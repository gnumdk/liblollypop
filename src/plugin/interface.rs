// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Interface to register plugins
//!
//! Plugin types:
//!  - Collection: provides music and artwork for a given namespace
//!  - View: provides a view for Lollypop sidebar
//!
//! Plugin interface
//!
//! Example:
//! ```no_run
//! use liblollypop::bridge::Bridge;
//! use liblollypop::plugin::interface::Plugin;
//! use liblollypop::plugin::interface::PluginInterface;
//! use liblollypop::plugin::{collection::CollectionPlugin, PluginPriority};
//! use liblollypop::providers::scanner::ScannerInterface;
//! use std::sync::Arc;
//! struct ScannerProvider;
//! impl ScannerInterface for ScannerProvider {
//!     fn update(&self, target_uris: Vec<String>) {
//!     }
//!     fn stop(&self) {
//!     }
//!     fn running(&self) -> bool {
//!         false
//!     }
//! }
//! struct MyCollection;
//! impl PluginInterface for MyCollection {
//!     fn plugin(&self, bridge: Arc<Bridge>) -> Plugin {
//!         let collection_plugin = CollectionPlugin::new(
//!             String::from("my_collection"),
//!             Box::new(ScannerProvider{}),
//!             PluginPriority::Web,
//!         );
//!         Plugin::Collection(collection_plugin)
//!     }
//!}
//! ```

use super::artwork::ArtworkPlugin;
use super::collection::CollectionPlugin;
use crate::bridge::Bridge;

use std::sync::Arc;

/// A plugin: collection or view
pub enum Plugin {
    Collection(CollectionPlugin),
    Artwork(ArtworkPlugin),
}

pub trait PluginInterface {
    // Get plugin
    fn plugin(&self, bridge: Arc<Bridge>) -> Plugin;
}
