// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use super::PluginPriority;
use crate::providers::scanner::ScannerInterface;

use std::boxed::Box;

/// A collection plugin provides music tracks to Lollypop
/// by implementing [`ScannerInteface`](crate::providers::scanner::ScannerInterface)
pub struct CollectionPlugin {
    pub id: String,
    pub interface: Box<dyn ScannerInterface>,
    pub priority: PluginPriority,
}

impl CollectionPlugin {
    /// Constructs a new CollectionPlugin
    /// # Parameters
    ///
    /// * `id`: Uniq plugin id
    /// * `interface`: Scanner interface used by Lollypop when user asks music to be updated
    /// * `priority`: Plugin priority used by Lollypop to sort plugins
    pub fn new(
        id: String,
        interface: Box<dyn ScannerInterface>,
        priority: PluginPriority,
    ) -> CollectionPlugin {
        CollectionPlugin {
            id,
            interface,
            priority,
        }
    }
}
