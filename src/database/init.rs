// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use super::sql::schema::{SCHEMA, UPGRADE};
use crate::storage_path;

use gio::prelude::*;
use gtk::gio;

use log::debug;
use rusqlite::Connection;
use std::fs::create_dir_all;
use std::path::Path;
use std::path::PathBuf;

pub fn init_namespace(namespace: &str, version: i32) -> bool {
    if !Path::new(storage_path::COLLECTION.as_str()).exists() {
        if let Err(err) = create_dir_all(storage_path::COLLECTION.as_str()) {
            panic!("Can't create directory: {}", err);
        }
    }
    let path = format!("{}/{}.db", storage_path::COLLECTION.as_str(), namespace);
    let f = gio::File::for_path(&path);
    if !f.query_exists(gio::Cancellable::NONE) {
        match Connection::open(&path) {
            Err(err) => panic!("Can't create database: {}", err),
            Ok(connection) => {
                for sql in &SCHEMA {
                    debug!("{}", sql);
                    connection.execute(&sql, []).unwrap();
                }
                let _ = connection.pragma_update(None, "user_version", version);
            }
        }
        return true;
    }
    false
}

// Only handle pure SQL requests for now
pub fn upgrade(namespace: &str) {
    let db_path = PathBuf::from(format!(
        "{}/{}.db",
        storage_path::COLLECTION.as_str(),
        namespace
    ));
    match Connection::open(&db_path) {
        Err(err) => panic!("Can't access database: {}", err),
        Ok(db) => {
            let version: i32 = db
                .query_row("PRAGMA user_version", [], |row| row.get(0))
                .ok()
                .unwrap();
            for i in version as usize..UPGRADE.len() {
                db.execute(&UPGRADE[i], []).unwrap();
            }
        }
    }
}
