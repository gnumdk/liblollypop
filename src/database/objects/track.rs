// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Represent a collection track

use crate::database::filters::Filters;
use crate::database::objects::properties::Property;
use crate::database::objects::relations::Relation;
use crate::database::sql::connection::Connection;

use std::collections::HashMap;

/// A track
pub struct Track<'a> {
    // Sql Connection
    pub connection: &'a Connection<'a>,
    // Properties
    pub properties: HashMap<Property, rusqlite::types::Value>,
    // Relations
    pub relations: HashMap<Relation, Vec<String>>,
    // Filters
    pub filters: Option<Filters>,
}

impl<'a> Track<'a> {
    /// Construct a new track
    /// # Parameters
    ///
    /// * `connection`: Connection to database
    /// * `id`: Track id
    /// * `filters`: Filters to apply
    pub fn new(connection: &'a Connection<'a>, id: String, filters: Option<Filters>) -> Track<'a> {
        let mut track = Track {
            connection,
            filters,
            properties: HashMap::new(),
            relations: HashMap::new(),
        };
        track
            .properties
            .insert(Property::Id, rusqlite::types::Value::Text(id));
        track
    }
}
