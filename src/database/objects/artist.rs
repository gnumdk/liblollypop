// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Represent a collection artist

use crate::database::filters::Filters;
use crate::database::objects::properties::Property;
use crate::database::objects::relations::Relation;
use crate::database::sql::connection::Connection;

use std::collections::HashMap;

/// An artist
pub struct Artist<'a> {
    // Sql Connection
    pub connection: &'a Connection<'a>,
    // Properties
    pub properties: HashMap<Property, rusqlite::types::Value>,
    // Relations
    pub relations: HashMap<Relation, Vec<String>>,
    // Filters
    pub filters: Option<Filters>,
}

impl<'a> Artist<'a> {
    /// Construct a new artist
    /// # Parameters
    ///
    /// * `connection`: Connection to database
    /// * `id`: Artist id
    /// * `filters`: Filters to apply
    pub fn new(connection: &'a Connection<'a>, id: String, filters: Option<Filters>) -> Artist<'a> {
        let mut artist = Artist {
            connection,
            filters,
            properties: HashMap::new(),
            relations: HashMap::new(),
        };
        artist
            .properties
            .insert(Property::Id, rusqlite::types::Value::Text(id));
        artist
    }
}
