// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Represent a collection album

use crate::database::filters::Filters;
use crate::database::objects::properties::Property;
use crate::database::objects::relations::Relation;
use crate::database::sql::connection::Connection;

use std::collections::HashMap;

/// An album
pub struct Album<'a> {
    /// Connection to database
    pub connection: &'a Connection<'a>,
    /// Use [`property`](crate::property) or [`property_option`](crate::property_option)
    pub properties: HashMap<Property, rusqlite::types::Value>,
    /// Use [`Relations`](crate::database::objects::relations::Relations)
    pub relations: HashMap<Relation, Vec<String>>,
    /// Filters applied to object Relations
    pub filters: Option<Filters>,
}

impl<'a> Album<'a> {
    /// Construct a new album
    /// # Parameters
    ///
    /// * `connection`: Connection to database
    /// * `id`: Album id
    /// * `filters`: Filters to apply
    pub fn new(connection: &'a Connection<'a>, id: String, filters: Option<Filters>) -> Album<'a> {
        let mut album = Album {
            connection,
            filters,
            properties: HashMap::new(),
            relations: HashMap::new(),
        };
        album
            .properties
            .insert(Property::Id, rusqlite::types::Value::Text(id));
        album
    }
}
