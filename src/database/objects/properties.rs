// Copyright (c) 2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! This trait is used to load objects properties

use crate::database::objects::album::Album;
use crate::database::objects::artist::Artist;
use crate::database::objects::disc::Disc;
use crate::database::objects::genre::Genre;
use crate::database::objects::track::Track;

use std::collections::HashMap;

#[derive(Debug, PartialEq, Eq, Hash)]
pub enum Property {
    Id,            // All
    Name,          // Artist
    Title,         // Album, Disc, Track
    Number,        // Disc, Track
    Sortname,      // Artist
    Synced,        // Album
    Mbid,          // Album, Artist
    Uri,           // Album, Track
    Popularity,    // Album, Track
    Status,        // Album, Track
    Rate,          // Album, Track
    Mtime,         // Track
    Year,          // ALbum, Track
    Timestamp,     // Album, Track
    Duration,      // Track
    Ltime,         // Track
    IsCompilation, // Album
    Bpm,           // Track
    DiscId,        // Track
    AlbumId,       // Track, Disc
}

macro_rules! impl_properties_loader {
    () => {
        fn load_properties(&mut self) -> &HashMap<Property, rusqlite::types::Value> {
            let lenght = self.properties.len();
            // If only id is Set
            if lenght == 1 {
                self.load();
            }
            &self.properties
        }
    };
}

use imp::ImplProperties;

pub trait Properties {
    fn load_properties(&mut self) -> &HashMap<Property, rusqlite::types::Value>;
}
impl<'a> Properties for Genre<'a> {
    impl_properties_loader!();
}
impl<'a> Properties for Artist<'a> {
    impl_properties_loader!();
}
impl<'a> Properties for Album<'a> {
    impl_properties_loader!();
}
impl<'a> Properties for Disc<'a> {
    impl_properties_loader!();
}
impl<'a> Properties for Track<'a> {
    impl_properties_loader!();
}

mod imp {
    use super::Album;
    use super::Artist;
    use super::Disc;
    use super::Genre;
    use super::Track;
    use crate::database::objects::properties::Property;
    use crate::database::sql::builder::{Query, Select, Where};
    use crate::database::sql::connection::{Sql, SqlValues};
    use crate::to_sql;

    use rusqlite::params;

    pub trait ImplProperties {
        fn load(&mut self);
    }
    impl<'a> ImplProperties for Genre<'a> {
        fn load(&mut self) {
            let mut query = Query {
                // Keep order in sync with Property
                select: Select::Sql("genres.name"),
                where_: Some(Where {
                    sql: "genres.id=?",
                    ..Default::default()
                }),
                ..Default::default()
            };
            let mut values = to_sql!(*self.connection)
                .row(&query.sql(), params![self.properties[&Property::Id]]);
            self.properties
                .insert(Property::Name, values.pop().unwrap());
        }
    }
    impl<'a> ImplProperties for Artist<'a> {
        fn load(&mut self) {
            let mut query = Query {
                // Keep order in sync with Property
                select: Select::Sql("artists.name, sortname, mbid"),
                where_: Some(Where {
                    sql: "artists.id=?",
                    ..Default::default()
                }),
                ..Default::default()
            };
            let mut values = to_sql!(*self.connection)
                .row(&query.sql(), params![self.properties[&Property::Id]]);
            self.properties.insert(
                Property::Mbid,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
            self.properties.insert(
                Property::Sortname,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
            self.properties.insert(
                Property::Name,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
        }
    }
    impl<'a> ImplProperties for Album<'a> {
        fn load(&mut self) {
            let mut query = Query {
                // Keep order in sync with Property
                select: Select::Sql(
                    "albums.title, mbid, uri, popularity, status, rate,\
                         year, timestamp, is_compilation, synced",
                ),
                where_: Some(Where {
                    sql: "albums.id=?",
                    ..Default::default()
                }),
                ..Default::default()
            };
            let mut values = to_sql!(*self.connection)
                .row(&query.sql(), params![self.properties[&Property::Id]]);
            self.properties.insert(
                Property::Synced,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::IsCompilation,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Timestamp,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Year,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Rate,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Status,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Popularity,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Uri,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
            self.properties.insert(
                Property::Mbid,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
            self.properties.insert(
                Property::Title,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
        }
    }
    impl<'a> ImplProperties for Disc<'a> {
        fn load(&mut self) {
            let mut query = Query {
                // Keep order in sync with Property
                select: Select::Sql("discs.title, album_id, number"),
                where_: Some(Where {
                    sql: "discs.id=?",
                    ..Default::default()
                }),
                ..Default::default()
            };
            let mut values = to_sql!(*self.connection)
                .row(&query.sql(), params![self.properties[&Property::Id]]);
            self.properties.insert(
                Property::Number,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::AlbumId,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
            self.properties.insert(
                Property::Title,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
        }
    }
    impl<'a> ImplProperties for Track<'a> {
        fn load(&mut self) {
            let mut query = Query {
                // Keep order in sync with Property
                select: Select::Sql(
                    "albums.title, number, mbid, uri, popularity, status, rate, mtime,\
                         year, timestamp, duration, ltime, album_id, disc_id, bpm",
                ),
                where_: Some(Where {
                    sql: "albums.id=?",
                    ..Default::default()
                }),
                ..Default::default()
            };
            let mut values = to_sql!(*self.connection)
                .row(&query.sql(), params![self.properties[&Property::Id]]);
            self.properties.insert(
                Property::Bpm,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::DiscId,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
            self.properties.insert(
                Property::AlbumId,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
            self.properties.insert(
                Property::Ltime,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Duration,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Timestamp,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Year,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Mtime,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Rate,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Status,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Popularity,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Uri,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
            self.properties.insert(
                Property::Mbid,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
            self.properties.insert(
                Property::Number,
                values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
            );
            self.properties.insert(
                Property::Title,
                values
                    .pop()
                    .unwrap_or(rusqlite::types::Value::Text(String::new())),
            );
        }
    }
}
