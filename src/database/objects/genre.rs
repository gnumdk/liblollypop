// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Represent a collection genre

use crate::database::objects::properties::Property;
use crate::database::objects::relations::Relation;
use crate::database::sql::connection::Connection;

use std::collections::HashMap;

/// A genre
pub struct Genre<'a> {
    // Sql Connection
    pub connection: &'a Connection<'a>,
    // Properties
    pub properties: HashMap<Property, rusqlite::types::Value>,
    // Relations
    pub relations: HashMap<Relation, Vec<String>>,
}

impl<'a> Genre<'a> {
    /// Construct a new genre
    /// # Parameters
    ///
    /// * `connection`: Connection to database
    /// * `id`: Genre id
    pub fn new(connection: &'a Connection<'a>, id: String) -> Genre<'a> {
        let mut genre = Genre {
            connection,
            properties: HashMap::new(),
            relations: HashMap::new(),
        };
        genre
            .properties
            .insert(Property::Id, rusqlite::types::Value::Text(id));
        genre
    }
}

impl std::fmt::Debug for Genre<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("Genre")
            //.field(&self.id)
            .field(&self.properties.get(&Property::Name).unwrap())
            .finish()
    }
}
