// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Represent a collection disc

use crate::database::filters::Filters;
use crate::database::objects::properties::Property;
use crate::database::objects::relations::Relation;
use crate::database::sql::connection::Connection;

use std::collections::HashMap;

/// A disc
pub struct Disc<'a> {
    // Sql Connection
    pub connection: &'a Connection<'a>,
    // True if skipped tracks are shown
    pub skipped_tracks: bool,
    // Properties
    pub properties: HashMap<Property, rusqlite::types::Value>,
    // Relations
    pub relations: HashMap<Relation, Vec<String>>,
    // Filters
    pub filters: Option<Filters>,
}

impl<'a> Disc<'a> {
    /// Construct a new disc
    /// # Parameters
    ///
    /// * `connection`: Connection to database
    /// * `id`: Disc id
    /// * `filters`: Filters to apply
    pub fn new(connection: &'a Connection<'a>, id: String, filters: Option<Filters>) -> Disc<'a> {
        let mut disc = Disc {
            connection,
            filters,
            skipped_tracks: true,
            properties: HashMap::new(),
            relations: HashMap::new(),
        };
        disc.properties
            .insert(Property::Id, rusqlite::types::Value::Text(id));
        disc
    }
}
