// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Object Macros

// Get values matching path in filters
#[macro_export]
macro_rules! values_for_filters {
    ($filters: expr, $matching: path) => {
        match &*$filters {
            Some(filters) => {
                for filter in filters {
                    match filter {
                        $matching(values) => {
                            return values.to_vec();
                        }
                        _ => (),
                    }
                }
            }
            None => (),
        }
        vec![]
    };
}

#[doc(hidden)]
#[macro_export]
macro_rules! property_from_object {
    ($obj: expr, $prop: expr, $t: tt) => {
        match $obj.load_properties().get($prop) {
            Some(rusqlite_value) => {
                $crate::to_value!(rusqlite_value, $t)
            }
            None => {
                log::error!("Can't load property for object: {:?}", $prop);
                $crate::to_value!(&rusqlite::types::Value::Null, $t)
            }
        }
    };
}

#[doc(hidden)]
#[macro_export]
macro_rules! property_option_from_object {
    ($obj: expr, $prop: expr, $t: tt) => {{
        $crate::to_option!($obj.load_properties().get($prop).unwrap(), $t)
    }};
}

// Get property
#[macro_export]
macro_rules! property {
    ($obj: expr, Property::Id) => {
        $crate::property_from_object![$obj, &Property::Id, str]
    };
    ($obj: expr, Property::Title) => {
        $crate::property_from_object![$obj, &Property::Title, str]
    };
    ($obj: expr, Property::Name) => {
        $crate::property_from_object![$obj, &Property::Name, str]
    };
    ($obj: expr, Property::Number) => {
        $crate::property_from_object![$obj, &Property::Number, i64]
    };
    ($obj: expr, Property::Sortname) => {
        $crate::property_from_object![$obj, &Property::Sortname, str]
    };
    ($obj: expr, Property::Synced) => {
        $crate::property_from_object![$obj, &Property::Synced, bool]
    };
    ($obj: expr, Property::Uri) => {
        $crate::property_from_object![$obj, &Property::Uri, str]
    };
    ($obj: expr, Property::Popularity) => {
        $crate::property_from_object![$obj, &Property::Popularity, i64]
    };
    ($obj: expr, Property::Status) => {
        $crate::property_from_object![$obj, &Property::Status, i64]
    };
    ($obj: expr, Property::Rate) => {
        $crate::property_from_object![$obj, &Property::Rate, i64]
    };
    ($obj: expr, Property::Mtime) => {
        $crate::property_from_object![$obj, &Property::Mtime, i64]
    };
    ($obj: expr, Property::Year) => {
        $crate::property_from_object![$obj, &Property::Year, i64]
    };
    ($obj: expr, Property::Timestamp) => {
        $crate::property_from_object![$obj, &Property::Timestamp, i64]
    };
    ($obj: expr, Property::Duration) => {
        $crate::property_from_object![$obj, &Property::Duration, f64]
    };
    ($obj: expr, Property::Ltime) => {
        $crate::property_from_object![$obj, &Property::Ltime, i64]
    };
    ($obj: expr, Property::IsCompilation) => {
        $crate::property_from_object![$obj, &Property::IsCompilation, bool]
    };
    ($obj: expr, Property::DetectedCompilation) => {
        $crate::property_from_object![$obj, &Property::DetectedCompilation, bool]
    };
    ($obj: expr, Property::Discid) => {
        $crate::property_from_object![$obj, &Property::DiscId, i64]
    };
    ($obj: expr, Property::Albumid) => {
        $crate::property_from_object![$obj, &Property::AlbumId, i64]
    };
}

// Get property as option
#[macro_export]
macro_rules! property_option {
    ($obj: expr, Property::Mbid) => {
        $crate::property_option_from_object![$obj, &Property::Mbid, str]
    };
    ($obj: expr, Property::Bpm) => {
        $crate::property_option_from_object![$obj, &Property::Bpm, i64]
    };
}
