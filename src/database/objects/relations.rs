// Copyright (c) 2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! This trait is used to load objects relations

use crate::database::objects::album::Album;
use crate::database::objects::artist::Artist;
use crate::database::objects::disc::Disc;
use crate::database::objects::track::Track;
use std::collections::HashMap;

/// Relation between objects in database
#[derive(PartialEq, Eq, Hash)]
pub enum Relation {
    ArtistIds,
    PerformerIds,
    ConductorIds,
    RemixerIds,
    ComposerIds,
    GenreIds,
    AlbumIds,
    DiscIds,
    TrackIds,
}

macro_rules! impl_relations_loader {
    () => {
        fn relation(&mut self, relation: Relation) -> Vec<String> {
            // Read lock here
            {
                if let Some(ids) = self.relations.get(&relation) {
                    return ids.clone();
                }
            }
            // Write lock here
            imp::Relations::load_relations(self, &relation);
            match self.relations.get(&relation) {
                Some(ids) => ids.clone(),
                None => Vec::new(),
            }
        }
    };
}

/// This trait allows to get relations between objects
pub trait Relations {
    /// Get relation ids
    fn relation(&mut self, relation: Relation) -> Vec<String>;
}

impl<'a> Relations for Artist<'a> {
    impl_relations_loader![];
}
impl<'a> Relations for Album<'a> {
    impl_relations_loader![];
}
impl<'a> Relations for Disc<'a> {
    impl_relations_loader![];
}
impl<'a> Relations for Track<'a> {
    impl_relations_loader![];
}

mod imp {
    use super::HashMap;
    use super::{Album, Artist, Disc, Track};
    use crate::database::filters::{Filter, Filters};
    use crate::database::objects::genre::Genre;
    use crate::database::objects::properties::{Properties, Property};
    use crate::database::sql::builder::{Object, Query, Select, Where};
    use crate::database::sql::connection::{Sql, SqlValues};
    use crate::{property, to_sql};

    use log::warn;
    use rusqlite::params;

    macro_rules! impl_relations_getter {
        () => {
            fn relations(&mut self) -> &mut HashMap<super::Relation, Vec<String>> {
                &mut self.relations
            }
        };
    }

    pub trait Relations {
        fn relations(&mut self) -> &mut HashMap<super::Relation, Vec<String>>;
        fn load_relations(&mut self, relation: &super::Relation) {
            match relation {
                super::Relation::GenreIds => {
                    let genre_ids = self.genre_ids();
                    self.relations()
                        .insert(super::Relation::GenreIds, genre_ids);
                }
                super::Relation::ArtistIds => {
                    let artist_ids = self.artist_ids();
                    self.relations()
                        .insert(super::Relation::ArtistIds, artist_ids);
                }
                super::Relation::AlbumIds => {
                    let album_ids = self.album_ids();
                    self.relations()
                        .insert(super::Relation::AlbumIds, album_ids);
                }
                super::Relation::DiscIds => {
                    let disc_ids = self.disc_ids();
                    self.relations().insert(super::Relation::DiscIds, disc_ids);
                }
                super::Relation::TrackIds => {
                    let track_ids = self.track_ids();
                    self.relations()
                        .insert(super::Relation::TrackIds, track_ids);
                }
                _ => {
                    warn!("Tracks::add_relation() called with invalid Relation");
                }
            }
        }
        fn genre_ids(&mut self) -> Vec<String> {
            vec![]
        }
        fn artist_ids(&mut self) -> Vec<String> {
            vec![]
        }
        fn album_ids(&mut self) -> Vec<String> {
            vec![]
        }
        fn disc_ids(&mut self) -> Vec<String> {
            vec![]
        }
        fn track_ids(&mut self) -> Vec<String> {
            vec![]
        }
    }
    impl<'a> Relations for Genre<'a> {
        impl_relations_getter![];
        fn artist_ids(&mut self) -> Vec<String> {
            let mut filters = Filters::new();
            filters.add(Filter::Genres(vec![property![self, Property::Id]]));
            let mut query = Query {
                select: Select::Object(Object::Artist),
                where_: Some(Where {
                    filters: Some(filters),
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            to_sql!(*self.connection).strings(&query.sql(), params![])
        }
    }
    impl<'a> Relations for Artist<'a> {
        impl_relations_getter![];
        fn album_ids(&mut self) -> Vec<String> {
            let mut filters = self.filters.clone().unwrap_or(Filters::new());
            filters.add(Filter::Artists(vec![property![self, Property::Id]]));
            let mut query = Query {
                select: Select::Object(Object::Album),
                where_: Some(Where {
                    filters: Some(filters),
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            to_sql!(*self.connection).strings(&query.sql(), &query.params())
        }
    }
    impl<'a> Relations for Album<'a> {
        impl_relations_getter![];
        // Get disc ids
        fn disc_ids(&mut self) -> Vec<String> {
            let mut query = Query {
                select: Select::Object(Object::Disc),
                where_: Some(Where {
                    sql: "discs.album_id=?",
                    filters: self.filters.clone(),
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            to_sql!(*self.connection).strings(&query.sql(), params![property![self, Property::Id]])
        }
        fn artist_ids(&mut self) -> Vec<String> {
            let mut query = Query {
                select: Select::Sql("album_artists.artist_id"),
                where_: Some(Where {
                    sql: "album_artists.album_id=?",
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            to_sql!(*self.connection).strings(&query.sql(), params![property![self, Property::Id]])
        }
    }
    impl<'a> Relations for Disc<'a> {
        impl_relations_getter![];
        fn artist_ids(&mut self) -> Vec<String> {
            let mut query = Query {
                select: Select::Sql("disc_artists.artist_id"),
                where_: Some(Where {
                    sql: "disc_artists.disc_id=?",
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            to_sql!(*self.connection).strings(&query.sql(), params![property![self, Property::Id]])
        }

        fn track_ids(&mut self) -> Vec<String> {
            let mut query = Query {
                select: Select::Object(Object::Track),
                where_: Some(Where {
                    sql: "tracks.disc_id=?",
                    filters: self.filters.clone(),
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            to_sql!(*self.connection).strings(
                &query.sql(),
                params![
                    property![self, Property::Id],
                    property![self, Property::Albumid]
                ],
            )
        }
    }
    impl<'a> Relations for Track<'a> {
        impl_relations_getter![];
    }
}
