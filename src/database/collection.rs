// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Collection manages database namespaces

use crate::database::init::*;

pub struct Collection {
    #[doc(hidden)]
    pub namespaces: Vec<String>,
}

impl Default for Collection {
    fn default() -> Collection {
        Self {
            namespaces: Vec::<String>::new(),
        }
    }
}

impl Collection {
    /// Construct a new collection
    pub fn new() -> Self {
        Collection {
            ..Default::default()
        }
    }
    /// Register a new plugin namespace
    pub fn register(&mut self, namespace: String) {
        if !init_namespace(&namespace, 0) {
            upgrade(&namespace);
        }
        self.namespaces.push(namespace);
    }

    /// Get registered plugins
    #[doc(hidden)]
    pub fn registered(&self) -> Vec<&str> {
        let mut registered = vec![];
        for namespace in &self.namespaces {
            registered.push(namespace.as_str());
        }
        registered
    }
}
