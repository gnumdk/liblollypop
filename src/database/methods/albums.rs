// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Albums database access methods

use crate::database::filters::{Filter, Filters};
use crate::database::objects::properties::Property;
use crate::database::sql::builder::{Object, OrderBy, Query, Select, Where};
use crate::database::sql::connection::{Sql, SqlValues};
use crate::tags::item::Item;
use crate::{timestamp, to_option, to_value, trim, StatusFlags};

use log::error;
use rusqlite::params;
use std::cmp::max;
use std::collections::HashMap;
use std::time::SystemTime;

/// Add a new album to namespace
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `properties`: Album properties
pub fn add(
    sql: &dyn Sql,
    properties: &HashMap<Property, rusqlite::types::Value>,
    artist_ids: &Vec<String>,
) -> Option<String> {
    let title = to_option!(
        properties
            .get(&Property::Title)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let year = to_option!(
        properties
            .get(&Property::Year)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let timestamp = to_option!(
        properties
            .get(&Property::Timestamp)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let mbid = to_option!(
        properties
            .get(&Property::Mbid)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let is_compilation = to_value!(
        properties
            .get(&Property::IsCompilation)
            .unwrap_or(&rusqlite::types::Value::Null),
        bool
    );
    let uri = to_value!(
        properties
            .get(&Property::Uri)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let status = to_value!(
        properties
            .get(&Property::Status)
            .unwrap_or(&rusqlite::types::Value::Integer(StatusFlags::NONE as i64)),
        i64
    );
    let popularity = to_value!(
        properties
            .get(&Property::Popularity)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let rate = to_value!(
        properties
            .get(&Property::Rate)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let synced = to_value!(
        properties
            .get(&Property::Synced)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let album_id_str = format!(
        "{:?}_{:?}_{:?}_{:?}",
        title,
        mbid,
        year,
        artist_ids.clone().sort()
    );
    let album_id = format!("{:x}", md5::compute(album_id_str.to_lowercase().as_bytes()));
    if exists(sql, &album_id) {
        return Some(album_id);
    }
    let query = "INSERT INTO albums (
                 id, title, mbid, is_compilation, uri, year,
                 timestamp, status, popularity, rate, synced)
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    match sql.execute(
        &query,
        params![
            album_id,
            trim!(title, Option<str>),
            trim!(mbid, Option<str>),
            is_compilation,
            uri,
            year,
            timestamp,
            status,
            popularity,
            rate,
            synced
        ],
    ) {
        Err(error) => {
            error!(
                "Failed to add album: {}/{}, {:?}, {:?}, {}, {}, {:?}, {:?}, {}, {}, {}, {} ({})",
                query,
                album_id,
                title,
                mbid,
                is_compilation,
                uri,
                year,
                timestamp,
                status,
                popularity,
                rate,
                synced,
                error
            );
            None
        }
        Ok(_) => Some(album_id),
    }
}

/// Add a new album item to namespace
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `item`: Album item
/// * `artist_ids`: album artist ids
pub fn add_item(sql: &dyn Sql, item: &Item, artist_ids: &Vec<String>) -> Option<String> {
    add(sql, &item.properties, artist_ids)
}

/// True if album id exists
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `id`: Album id
pub fn exists(sql: &dyn Sql, id: &str) -> bool {
    let mut filters = Filters::new();
    filters.add(Filter::Id(String::from(id)));
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            filters: Some(filters),
            ..Default::default()
        }),
        ..Default::default()
    };
    let result = sql.string_option(&query.sql(), query.params().as_slice());
    result != None
}

/// Return album ids
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `allow_compilations`: return compilations in results
/// * `status_flags`: Album status
/// * `limit`: Results limit
pub fn ids(
    sql: &dyn Sql,
    filters: Option<Filters>,
    orderby: Option<OrderBy>,
    allow_compilations: bool,
    status_flagss: StatusFlags,
    limit: Option<i64>,
) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        params: vec![
            rusqlite::types::Value::Integer(allow_compilations as i64),
            rusqlite::types::Value::Integer(status_flagss as i64),
        ],
        where_: Some(Where {
            sql: "albums.is_compilation=? \
                      AND albums.status&?",
            filters,
            ..Default::default()
        }),
        orderby,
        limit,
        distinct: true,
        ..Default::default()
    };
    sql.strings(&query.sql(), &query.params())
}

/// Return album ids with rate >= 4
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `status_flags`: Album status
/// * `limit`: Results limit
pub fn rated_ids(
    sql: &dyn Sql,
    filters: Option<Filters>,
    status_flags: i64,
    limit: Option<i64>,
) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            sql: "rate>=4 AND albums.status&?",
            filters,
            ..Default::default()
        }),
        limit,
        orderby: Some(OrderBy::Custom("rate DESC")),
        ..Default::default()
    };
    sql.strings(&query.sql(), params![status_flags])
}

/// Return album ids with popularity != 0
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `status_flags`: Album status
/// * `limit`: Results limit
pub fn popular_ids(
    sql: &dyn Sql,
    filters: Option<Filters>,
    status_flags: i64,
    limit: Option<i64>,
) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            sql: "popularity!=0 AND albums.status&?",
            filters,
            ..Default::default()
        }),
        limit,
        orderby: Some(OrderBy::Custom("popularity DESC")),
        ..Default::default()
    };
    sql.strings(&query.sql(), params![status_flags])
}

/// Return album ids ordered by timed popularity
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `status_flags`: Album status
/// * `limit`: Results limit
pub fn popular_ids_currently(
    sql: &dyn Sql,
    filters: Option<Filters>,
    status_flags: i64,
    limit: Option<i64>,
) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            sql: "albums.id = albums_timed_popularity.album_id AND albums.status&?",
            filters,
            ..Default::default()
        }),
        orderby: Some(OrderBy::Custom("albums_timed_popularity.popularity DESC")),
        limit,
        ..Default::default()
    };
    sql.strings(&query.sql(), params![status_flags])
}

/// Get synced album ids for device
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `device_id`: target device id
pub fn synced_ids(sql: &dyn Sql, device_id: i64) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            sql: "synced&(1<<?)",
            ..Default::default()
        }),
        ..Default::default()
    };
    sql.strings(&query.sql(), params![device_id])
}

/// Remove device from albums synced state
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `device_id`: target device id
#[allow(unused_must_use)]
pub fn remove_device(sql: &dyn Sql, device_id: i64) {
    sql.execute(
        "UPDATE albums SET synced = synced & ~(1<<?)",
        params![device_id],
    );
}

/// Return album ids matching search
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `limit`: Results limit
pub fn search(sql: &dyn Sql, filter: &str, limit: i64) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            sql: "no_accents(title) LIKE %?%",
            ..Default::default()
        }),
        limit: Some(limit),
        ..Default::default()
    };
    sql.strings(&query.sql(), params![filter])
}

/// Get albums count
///
/// # Parameters
///
/// * `sql`: Connection to database
pub fn count(sql: &dyn Sql) -> i64 {
    let mut query = Query {
        select: Select::Sql("COUNT(albums.*)"),
        ..Default::default()
    };
    sql.integer(&query.sql(), params![])
}

/// Set album popularity
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `album_id`: album id
/// * `popularity`: album popularity
#[allow(unused_must_use)]
pub fn set_popularity(sql: &dyn Sql, album_id: String, popularity: i64) {
    sql.execute(
        "UPDATE albums set popularity=? WHERE id=?",
        params![popularity, album_id],
    );
}

/// Increase album popularity
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `album_id`: album id
pub fn increase_popularity(sql: &dyn Sql, album_id: String) {
    let mut max_query = Query {
        select: Select::Sql("albums.popularity"),
        orderby: Some(OrderBy::Custom("POPULARITY DESC")),
        limit: Some(1),
        ..Default::default()
    };
    let mut current_query = Query {
        select: Select::Sql("albums.popularity"),
        where_: Some(Where {
            sql: "albums.id=?",
            ..Default::default()
        }),
        ..Default::default()
    };
    let max_popularity = sql.integer(&max_query.sql(), params![]);
    let current_popularity = sql.integer(&current_query.sql(), params![album_id]);
    // 100 playbacks should make an album as popular as most popular one
    let increment = (max_popularity - current_popularity) / 100;
    set_popularity(sql, album_id, max(1, increment));
}

/// Clean albums table for all available namespaces
///
/// # Parameters
///
/// * `sql`: Connection to database
#[allow(unused_must_use)]
pub fn clean(sql: &dyn Sql) {
    sql.execute(
        "DELETE FROM albums
            WHERE albums.id NOT IN (
                SELECT album_id FROM tracks)",
        params![],
    );
    sql.execute(
        "DELETE FROM albums_timed_popularity
               WHERE albums_timed_popularity.album_id NOT IN (
                SELECT albums.id FROM albums)",
        params![],
    );
    // We clear timed popularity based on mtime
    // Don't keep more data than a month
    let month = timestamp![SystemTime::now()] - 2678400;
    sql.execute(
        "DELETE FROM albums_timed_popularity
               WHERE albums_timed_popularity.mtime < ?",
        params![month as i64],
    );
    sql.execute("DELETE FROM album_artists", params![]);
    // First insert disc artists as it
    sql.execute(
        "INSERT INTO album_artists
            SELECT DISTINCT discs.album_id, disc_artists.artist_id
            FROM disc_artists, discs
            WHERE disc_artists.disc_id=discs.id",
        params![],
    );
    // Than try to detect albums without album artist tag and with only one track artist
    sql.execute(
        "INSERT INTO album_artists
            SELECT DISTINCT discs.album_id, track_artists.artist_id
            FROM discs, tracks, track_artists
               WHERE tracks.disc_id=discs.id
               AND track_artists.track_id = tracks.id
               AND NOT EXISTS
                   (SELECT disc_artists.artist_id
                    FROM disc_artists
                    WHERE discs.id=disc_artists.disc_id)
               AND (SELECT COUNT(DISTINCT track_artists.artist_id)
                    FROM track_artists, tracks
                    WHERE tracks.id=track_artists.track_id
                    AND discs.id=tracks.disc_id) = 1 ORDER BY track_artists.artist_id",
        params![],
    );
}
