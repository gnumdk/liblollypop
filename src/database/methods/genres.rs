// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Genres database access methods

use crate::database::filters::{Filter, Filters};
use crate::database::objects::properties::Property;
use crate::database::sql::builder::{Object, OrderBy, Query, Select, Where};
use crate::database::sql::connection::{Sql, SqlValues};
use crate::tags::item::Item;
use crate::{to_value, trim};

use log::error;
use rusqlite::params;
use std::collections::HashMap;

/// Add a new genre to namespace
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `properties`: Genre properties
pub fn add(
    sql: &dyn Sql,
    properties: &HashMap<Property, rusqlite::types::Value>,
) -> Option<String> {
    let name = to_value![
        properties
            .get(&Property::Name)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    ];
    let genre_id = crate::utils::strings::sqlescape(&name);
    if exists(sql, &genre_id) {
        return Some(genre_id);
    }
    let query = format!["INSERT INTO genres (id, name) VALUES (?, ?)"];
    match sql.execute(&query, params![genre_id, trim!(name, str)]) {
        Err(error) => {
            error!(
                "Failed to add genre: {}/{}, {} ({})",
                query, genre_id, name, error
            );
            None
        }
        Ok(_) => Some(genre_id),
    }
}

/// Add a new genre item to namespace
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `genre_item`: Genre item
pub fn add_item(sql: &dyn Sql, item: &Item) -> Option<String> {
    add(sql, &item.properties)
}

/// True if genre id exists
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `id`: Genre id
pub fn exists(sql: &dyn Sql, id: &str) -> bool {
    let mut filters = Filters::new();
    filters.add(Filter::Id(String::from(id)));
    let mut query = Query {
        select: Select::Object(Object::Genre),
        where_: Some(Where {
            filters: Some(filters),
            ..Default::default()
        }),
        ..Default::default()
    };
    let result = sql.string_option(&query.sql(), query.params().as_slice());
    result != None
}

/// Genre ids
///
/// # Parameters
///
/// * `sql`: Connection to database
pub fn ids(sql: &dyn Sql) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Genre),
        orderby: Some(OrderBy::Custom("genres.name COLLATE LOCALIZED")),
        ..Default::default()
    };
    sql.strings(&query.sql(), params![])
}

/// Return random genre ids
///
/// # Parameters
///
/// * `sql`: Connection to database
pub fn random_ids(sql: &dyn Sql) -> Vec<i64> {
    let mut query = Query {
        select: Select::Object(Object::Genre),
        where_: Some(Where {
            ..Default::default()
        }),
        orderby: Some(OrderBy::Custom("RANDOM()")),
        ..Default::default()
    };
    sql.integers(&query.sql(), params![])
}

/// Clean genre table for all available namespaces
///
/// # Parameters
///
/// * `sql`: Connection to database
#[allow(unused_must_use)]
pub fn clean(sql: &dyn Sql) {
    sql.execute(
        "DELETE FROM genres WHERE genres.id NOT IN (
               SELECT disc_genres.genre_id
               FROM disc_genres",
        params![],
    );
    sql.execute(
        "DELETE FROM genres WHERE genres.id NOT IN (
                SELECT track_genres.genre_id
                FROM track_genres",
        params![],
    );
}
