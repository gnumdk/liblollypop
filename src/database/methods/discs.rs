// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Discs database access methods

use crate::database::filters::{Filter, Filters};
use crate::database::objects::properties::Property;
use crate::database::objects::relations::Relation;
use crate::database::sql::builder::{Object, Query, Select, Where};
use crate::database::sql::connection::{Sql, SqlValues};
use crate::tags::item::DiscItem;
use crate::{to_option, to_value, trim};

use log::{error, warn};
use rusqlite::params;
use std::collections::HashMap;

/// Add a new disc to namespace
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `properties`: Disc properties
pub fn add(
    sql: &dyn Sql,
    properties: &HashMap<Property, rusqlite::types::Value>,
) -> Option<String> {
    let title = to_option!(
        properties
            .get(&Property::Title)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let number = to_value!(
        properties
            .get(&Property::Number)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let album_id = to_value!(
        properties
            .get(&Property::AlbumId)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let disc_id_str = format!("{:?}_{}_{}", title, number, album_id);
    let disc_id = format!("{:x}", md5::compute(disc_id_str.to_lowercase().as_bytes()));
    if exists(sql, &disc_id) {
        return Some(disc_id);
    }
    let query = "INSERT INTO discs (id, title, number, album_id) VALUES (?, ?, ?, ?)";
    match sql.execute(
        &query,
        params![disc_id, trim!(title, Option<str>), number, album_id],
    ) {
        Err(error) => {
            error!(
                "Failed to add disc: {}/{},{:?},{},{} ({})",
                query, disc_id, title, number, album_id, error
            );
            None
        }
        Ok(_) => Some(disc_id),
    }
}

/// Add relation to disc
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `disc_id`: Target disc id
/// * `ids`: ids Related ids
/// * `relation`: Relation
pub fn add_relation(sql: &dyn Sql, disc_id: &String, ids: &Vec<String>, relation: Relation) {
    match relation {
        Relation::ArtistIds => {
            for artist_id in ids {
                let query = format!(
                    "REPLACE INTO disc_artists (disc_id, artist_id)
                                     VALUES (?, ?)"
                );
                match sql.execute(query.as_str(), params![disc_id, artist_id]) {
                    Err(error) => error!("Failed to add artist to disc: {}", error),
                    Ok(_rowid) => {}
                }
            }
        }
        Relation::GenreIds => {
            for genre_id in ids {
                let query = format!(
                    "REPLACE INTO disc_genres (disc_id, genre_id)
                                     VALUES (?, ?)"
                );
                match sql.execute(query.as_str(), params![disc_id, genre_id]) {
                    Err(error) => error!("Failed to add genre to disc: {}", error),
                    Ok(_rowid) => {}
                }
            }
        }
        _ => warn!("Discs::add_relation() called with invalid Relation"),
    }
}

/// Add a new album item to namespace
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `item`: Disc item
pub fn add_item(sql: &dyn Sql, item: &DiscItem) -> Option<String> {
    add(sql, &item.properties)
}

/// True if disc id exists
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `id`: Disc id
pub fn exists(sql: &dyn Sql, id: &str) -> bool {
    let mut filters = Filters::new();
    filters.add(Filter::Id(String::from(id)));
    let mut query = Query {
        select: Select::Object(Object::Disc),
        where_: Some(Where {
            filters: Some(filters),
            ..Default::default()
        }),
        ..Default::default()
    };
    let result = sql.string_option(&query.sql(), query.params().as_slice());
    result != None
}

/// Clean discs table for all available namespaces
///
/// # Parameters
///
/// * `sql`: Connection to database
#[allow(unused_must_use)]
pub fn clean(sql: &dyn Sql) {
    sql.execute(
        "DELETE FROM discs WHERE discs.id NOT IN (
         SELECT disc_id FROM tracks)",
        params![],
    );
    sql.execute(
        "DELETE FROM disc_genres
            WHERE disc_genres.disc_id NOT IN (
                SELECT discs.id FROM discs)",
        params![],
    );
    sql.execute(
        "DELETE FROM disc_artists
            WHERE disc_artists.disc_id NOT IN (
                SELECT discs.id FROM discs)",
        params![],
    );
}
