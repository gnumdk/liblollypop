// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Useful database access methods

pub mod albums;
pub mod artists;
pub mod discs;
pub mod genres;
pub mod tracks;
