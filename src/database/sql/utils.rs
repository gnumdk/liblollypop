// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use crate::storage_path;

use log::{debug, error};
use rusqlite::Connection;

fn strcoll(a: &str, b: &str) -> std::cmp::Ordering {
    let c_a = std::ffi::CString::new(a).expect("CString::new failed");
    let c_b = std::ffi::CString::new(b).expect("CString::new failed");
    let result: i32;
    unsafe {
        result = libc::strcoll(c_a.as_ptr(), c_b.as_ptr());
    }
    if result > 0 {
        std::cmp::Ordering::Greater
    } else if result < 0 {
        std::cmp::Ordering::Less
    } else {
        std::cmp::Ordering::Equal
    }
}

fn localized_collation(connection: &Connection) {
    connection.create_collation("LOCALIZED", strcoll).ok();
}

fn no_accents(connection: &Connection) {
    if let Err(error) = connection.create_scalar_function(
        "noaccents",
        1,
        rusqlite::functions::FunctionFlags::SQLITE_UTF8
            | rusqlite::functions::FunctionFlags::SQLITE_DETERMINISTIC,
        |ctx| {
            let value = ctx.get::<String>(0).unwrap();
            Ok(crate::utils::strings::noaccents(value.as_str()))
        },
    ) {
        error!("Can't create noaccents() function: {}", error);
    }
}

fn sqlescape(connection: &Connection) {
    if let Err(error) = connection.create_scalar_function(
        "sqlescape",
        1,
        rusqlite::functions::FunctionFlags::SQLITE_UTF8
            | rusqlite::functions::FunctionFlags::SQLITE_DETERMINISTIC,
        |ctx| {
            let value = ctx.get::<String>(0).unwrap();
            Ok(crate::utils::strings::sqlescape(value.as_str()))
        },
    ) {
        error!("Can't create sqlescape() function: {}", error);
    }
}

pub fn get_connection(namespace: &str) -> Connection {
    let path = format!("{}/{}.db", storage_path::COLLECTION.as_str(), namespace);
    match Connection::open(&path) {
        Ok(connection) => {
            localized_collation(&connection);
            no_accents(&connection);
            sqlescape(&connection);
            connection
        }
        _ => {
            match Connection::open_in_memory() {
                Ok(connection) => {
                    // Inject schema in memory to let application run without any namespace
                    for sql in &crate::database::sql::schema::SCHEMA {
                        connection.execute(&sql, []).unwrap();
                    }
                    connection
                }
                Err(error) => {
                    panic!("Failed to access SQLite: {}", error);
                }
            }
        }
    }
}

pub fn get_ro_connection(namespaces: &Vec<&str>) -> Connection {
    let path = storage_path::COLLECTION.as_str();
    match Connection::open_in_memory() {
        Ok(connection) => {
            if namespaces.len() > 0 {
                for namespace in namespaces.iter() {
                    let sql = format!("ATTACH DATABASE '{0}/{1}.db' AS {1}", path, namespace);
                    debug!("{}", sql);
                    match connection.execute(sql.as_str(), []) {
                        Ok(_) => debug!("Attached database"),
                        Err(err) => error!("Attach failed: {}", err),
                    }
                }
                for table in [
                    "genres",
                    "artists",
                    "albums",
                    "discs",
                    "tracks",
                    "album_artists",
                    "disc_artists",
                    "disc_genres",
                    "albums_timed_popularity",
                    "featuring",
                    "track_genres",
                    "track_artists",
                    "track_album_artists",
                    "track_performers",
                    "track_remixes",
                    "track_conductors",
                    "track_composers",
                ] {
                    let mut views = vec![];
                    let mut sql = format!("CREATE TEMP VIEW {} AS ", table);
                    for namespace in namespaces.iter() {
                        views.push(format![
                            "SELECT *, '{0}' as namespace FROM {0}.{1}",
                            namespace, table
                        ]);
                    }
                    sql.push_str(&views.join(" UNION "));
                    match connection.execute(&sql, []) {
                        Ok(_) => debug!("Views created: {}", &sql),
                        Err(err) => error!("View creation failed: {}", err),
                    }
                }
            } else {
                // Inject schema in memory to let application run without any namespace
                for sql in &crate::database::sql::schema::SCHEMA {
                    connection.execute(&sql, []).unwrap();
                }
            }

            localized_collation(&connection);
            no_accents(&connection);
            sqlescape(&connection);
            connection
        }
        Err(error) => {
            panic!("Failed to access SQLite: {}", error);
        }
    }
}
