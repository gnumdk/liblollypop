// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Simple query builder

use crate::database::filters::{Filter, Filters};
use crate::params_from_values;
use crate::utils::strings::{noaccents, sqlescape};

pub enum Object {
    Genre,
    Artist,
    Album,
    Disc,
    Track,
}

bitflags::bitflags! {
    #[derive(Clone, Copy, PartialEq, Eq)]
    pub struct Join: u32 {
        const NONE                  = 0b00000001;
        const ARTIST_ALBUM          = 0b00000010;
        const ARTIST_DISC           = 0b00000100;
        const ARTIST_TRACK          = 0b00001000;
        const GENRE_DISC            = 0b00010000;
        const GENRE_TRACK           = 0b00100000;
        const ALBUM_DISC            = 0b01000000;
        const GENRE_DISC_ARTISTS    = 0b10000000;
        const GENRE_TRACK_ARTISTS   = 0b10000001;
    }
}

impl Default for Join {
    fn default() -> Join {
        Join::NONE
    }
}

pub enum Select<'a> {
    Sql(&'a str),
    Object(Object),
}

impl<'a> Default for Select<'a> {
    fn default() -> Select<'a> {
        Select::Sql("")
    }
}

/// Represent a SQL Query
#[derive(Default)]
pub struct Query<'a> {
    pub select: Select<'a>,
    pub where_: Option<Where<'a>>,
    pub params: Vec<rusqlite::types::Value>,
    pub orderby: Option<OrderBy<'a>>,
    pub groupby: Option<&'a str>,
    pub limit: Option<i64>,
    pub distinct: bool,
}

/// Represent a SQL WHERE
#[derive(Default)]
pub struct Where<'a> {
    pub sql: &'a str,
    pub filters: Option<Filters>,
}

/// For albums sort, use custom otherwise
pub enum OrderBy<'a> {
    ArtistYear,
    Artist,
    Title,
    Popularity,
    YearDesc,
    YearAsc,
    Custom(&'a str),
}

impl Object {
    pub fn table(&self) -> &str {
        match self {
            Object::Genre => "genres",
            Object::Artist => "artists",
            Object::Album => "albums",
            Object::Disc => "discs",
            Object::Track => "tracks",
        }
    }
}

impl Query<'_> {
    pub fn sql(&mut self) -> String {
        let mut sql: String;
        // Get Select
        let mut select;
        if self.distinct {
            select = String::from("SELECT DISTINCT ");
        } else {
            select = String::from("SELECT ");
        }
        let mut where_object: Option<&Object> = None;
        match &self.select {
            Select::Sql(sql) => {
                select.push_str(sql);
            }
            Select::Object(object) => {
                where_object = Some(object);
                let table = object.table();
                select.push_str(table);
                select.push_str(".id");
            }
        }
        // Insert namespace in result
        sql = format!("{} FROM @FROM@ ", select);

        if let Some(where_) = &mut self.where_ {
            let (where_sql, mut params) = where_.sql(where_object, &self.orderby);
            sql.push_str(where_sql.as_str());
            self.params.append(&mut params);
        }
        if let Some(orderby) = &self.orderby {
            sql.push_str(format!(" ORDER BY {}", orderby.as_str()).as_str());
        }
        if let Some(groupby) = &self.groupby {
            sql.push_str(format!(" GROUP BY {}", groupby).as_str());
        }
        if let Some(limit) = &self.limit {
            sql.push_str(format!(" LIMIT {}", limit).as_str());
        }
        let tables = self.tables(&sql);
        sql.replace("@FROM@", tables.join(",").as_str())
    }

    pub fn params(&self) -> Vec<&dyn rusqlite::ToSql> {
        params_from_values!(self.params)
    }

    fn tables(&self, query: &String) -> Vec<&str> {
        let mut tables = Vec::new();
        if query.contains("track_artists.") {
            tables.push("track_artists");
        }
        if query.contains("track_genres.") {
            tables.push("track_genres");
        }
        if query.contains("album_artists.") {
            tables.push("album_artists");
        }
        if query.contains("disc_artists.") {
            tables.push("disc_artists");
        }
        if query.contains("disc_genres.") {
            tables.push("disc_genres");
        }
        if query.contains("albums_timed_popularity") {
            tables.push("albums_timed_popularity");
        }
        if query.contains("genres.") {
            tables.push("genres");
        }
        if query.contains("artists.") {
            tables.push("artists");
        }
        if query.contains("albums.") {
            tables.push("albums");
        }
        if query.contains("discs.") {
            tables.push("discs");
        }
        if query.contains("tracks.") {
            tables.push("tracks");
        }
        tables
    }
}

impl Join {
    pub fn add_order_by(&mut self, orderby: &Option<OrderBy>) {
        match orderby {
            Some(value) => match value {
                OrderBy::ArtistYear | OrderBy::Artist => {
                    *self = *self | Join::ARTIST_ALBUM;
                }
                _ => {}
            },
            None => {}
        }
    }

    pub fn as_strs(&self) -> Vec<&str> {
        let mut strings = vec![];
        if *self & Join::ARTIST_ALBUM == Join::ARTIST_ALBUM {
            strings.push("albums.id=album_artists.album_id AND album_artists.artist_id=artists.id");
        }
        if *self & Join::ARTIST_DISC == Join::ARTIST_DISC {
            strings.push("discs.id=disc_artists.disc_id AND disc_artists.artist_id=artists.id");
        }
        if *self & Join::ARTIST_TRACK == Join::ARTIST_TRACK {
            strings.push("tracks.id=track_artists.track_id AND track_artists.artist_id=artists.id");
        }
        if *self & Join::GENRE_DISC == Join::GENRE_DISC {
            strings.push("discs.id=disc_genres.disc_id AND disc_genres.genre_id=genres.id");
        }
        if *self & Join::GENRE_TRACK == Join::GENRE_TRACK {
            strings.push("tracks.id=track_genres.track_id AND track_genres.genre_id=genres.id");
        }
        if *self & Join::ALBUM_DISC == Join::ALBUM_DISC {
            strings.push("albums.id=discs.album_id");
        }
        if *self & Join::GENRE_DISC_ARTISTS == Join::GENRE_DISC_ARTISTS {
            strings.push(
                "genres.id=disc_genres.genre_id AND disc_artists.disc_id=disc_genres.disc_id",
            );
        }
        if *self & Join::GENRE_TRACK_ARTISTS == Join::GENRE_TRACK_ARTISTS {
            strings.push(
                "genres.id=track_genres.genre_id AND track_artists.track_id=track_genres.track_id",
            );
        }
        strings
    }
}

impl<'a> OrderBy<'a> {
    fn as_str(&self) -> &'a str {
        match self {
            OrderBy::ArtistYear => {
                "artists.sortname COLLATE LOCALIZED, \
                     albums.timestamp, \
                     albums.title COLLATE LOCALIZED"
            }
            OrderBy::Artist => {
                "artists.sortname COLLATE LOCALIZED, \
                     albums.title COLLATE LOCALIZED"
            }
            OrderBy::Title => "albums.title COLLATE LOCALIZED",
            OrderBy::Popularity => {
                "albums.timestamp DESC, \
                     albums.title COLLATE LOCALIZED"
            }
            OrderBy::YearDesc => {
                "albums.timestamp DESC, \
                    albums.title COLLATE LOCALIZED"
            }
            OrderBy::YearAsc => {
                "albums.timestamp ASC, \
                    albums.title COLLATE NOCASE"
            }
            OrderBy::Custom(value) => value,
        }
    }
}

impl Where<'_> {
    pub fn sql(
        &mut self,
        object: Option<&Object>,
        orderby: &Option<OrderBy>,
    ) -> (String, Vec<rusqlite::types::Value>) {
        let mut filters = vec![];
        let mut params = vec![];
        let mut sql: String;
        let mut sql_params: Vec<rusqlite::types::Value> = Vec::new();
        let mut join = Join::NONE;
        if let Some(object) = object {
            let (filters_, params_) = self.mkfilters(object, &mut join);
            filters = filters_;
            params = params_;
        }
        join.add_order_by(orderby);
        let join_strings = join.as_strs();
        if filters.len() > 0 || join_strings.len() > 0 || self.sql.len() > 0 {
            sql = String::from("WHERE ");
        } else {
            sql = String::new();
        }
        if self.sql.len() > 0 {
            filters.insert(0, String::from(self.sql));
        }
        if filters.len() > 0 {
            let filters_sql = filters.join(" AND ");
            sql.push_str(filters_sql.as_str());
            sql_params.append(&mut params);
        }

        if join_strings.len() > 0 {
            let join_sql = join_strings.join(" AND ");
            sql.push_str(" AND ");
            sql.push_str(join_sql.as_str());
        }
        (sql, sql_params)
    }

    fn mkfilters(
        &mut self,
        object: &Object,
        join: &mut Join,
    ) -> (Vec<String>, Vec<rusqlite::types::Value>) {
        let mut sql = Vec::new();
        let mut params = Vec::new();
        let filters: Vec<Filter>;
        if let Some(filters_) = &self.filters {
            filters = filters_.to_vec();
        } else {
            return (sql, params);
        }
        for filter in &filters {
            if let Filter::Id(id) = filter {
                sql.push(format!("{}.id=?", object.table()));
                params.push(rusqlite::types::Value::Text(String::from(id)));
            }
            if let Filter::Name(Some(name)) = filter {
                sql.push(format!("{}.name=?", object.table()));
                params.push(rusqlite::types::Value::Text(String::from(name)));
            }
            if let Filter::EscapedName(Some(name)) = filter {
                sql.push(format!("sqlescape({}.name)=?", object.table()));
                params.push(rusqlite::types::Value::Text(sqlescape(&name)));
            }
            if let Filter::NoAccentName(Some(name)) = filter {
                sql.push(format!("noaccents({}.name)=?", object.table()));
                params.push(rusqlite::types::Value::Text(noaccents(&name)));
            }
            if let Filter::Title(Some(title)) = filter {
                sql.push(format!("{}.title=?", object.table()));
                params.push(rusqlite::types::Value::Text(String::from(title)));
            }
            if let Filter::EscapedTitle(Some(title)) = filter {
                sql.push(format!("sqlescape({}.title)=?", object.table()));
                params.push(rusqlite::types::Value::Text(sqlescape(&title)));
            }
            if let Filter::NoAccentTitle(Some(title)) = filter {
                sql.push(format!("noaccents({}.title)=?", object.table()));
                params.push(rusqlite::types::Value::Text(noaccents(&title)));
            }
            if let Filter::Number(Some(number)) = filter {
                sql.push(format!("{}.number=?", object.table()));
                params.push(rusqlite::types::Value::Integer(*number));
            }
            if let Filter::Album(Some(album_id)) = filter {
                sql.push(format!("{}.album_id=?", object.table()));
                params.push(rusqlite::types::Value::Integer(*album_id));
            }
            if let Filter::Mbid(Some(mbid)) = filter {
                sql.push(format!("{}.mbid=?", object.table()));
                params.push(rusqlite::types::Value::Text(String::from(mbid)));
            }
            if let Filter::Timestamp(Some(timestamp)) = filter {
                sql.push(format!("{}.timestamp=?", object.table()));
                params.push(rusqlite::types::Value::Integer(*timestamp));
            }
            if let Filter::Uri(Some(uri)) = filter {
                sql.push(format!("{}.uri=?", object.table()));
                params.push(rusqlite::types::Value::Text(String::from(uri)));
            }
            if let Filter::Genres(genre_ids) = filter {
                if genre_ids.len() == 0 {
                    continue;
                }
                for genre_id in genre_ids {
                    params.push(rusqlite::types::Value::Text(genre_id.clone()));
                }
                match object {
                    Object::Artist => {
                        //TODO Handle option to show all artists
                        if true {
                            *join |= Join::GENRE_DISC | Join::ALBUM_DISC;
                            sql.push(self.sqlfilter("disc_genres.genre_id", genre_ids, "OR"));
                        } else {
                            *join |= Join::GENRE_TRACK;
                            sql.push(self.sqlfilter("track_genres.genre_id", genre_ids, "OR"));
                        }
                    }
                    Object::Album => {
                        *join |= Join::GENRE_DISC | Join::ALBUM_DISC;
                        sql.push(self.sqlfilter("disc_genres.genre_id", genre_ids, "OR"));
                    }
                    Object::Disc => {
                        *join |= Join::GENRE_DISC;
                        sql.push(self.sqlfilter("disc_genres.genre_id", genre_ids, "OR"));
                    }
                    Object::Track => {
                        *join |= Join::GENRE_TRACK;
                        sql.push(self.sqlfilter("track_genres.genre_id", genre_ids, "OR"));
                    }
                    _ => (),
                }
            }
            if let Filter::Artists(artist_ids) = filter {
                if artist_ids.len() == 0 {
                    continue;
                }
                for artist_id in artist_ids {
                    params.push(rusqlite::types::Value::Text(artist_id.clone()));
                }
                match object {
                    Object::Genre => {
                        //TODO Handle option to show all artists
                        if true {
                            *join |= Join::GENRE_DISC_ARTISTS;
                        } else {
                            *join |= Join::GENRE_TRACK_ARTISTS;
                        }
                    }
                    Object::Album => {
                        *join |= Join::ARTIST_ALBUM;
                        sql.push(self.sqlfilter("album_artists.artist_id", artist_ids, "OR"));
                    }
                    Object::Disc => {
                        *join |= Join::ARTIST_DISC;
                        sql.push(self.sqlfilter("disc_artists.artist_id", artist_ids, "OR"));
                    }
                    Object::Track => {
                        *join |= Join::ARTIST_TRACK;
                        sql.push(self.sqlfilter("track_artists.artist_id", artist_ids, "OR"));
                    }
                    _ => (),
                }
            }
            if let Filter::Discs(disc_ids) = filter {
                if disc_ids.len() == 0 {
                    continue;
                }
                for disc_id in disc_ids {
                    params.push(rusqlite::types::Value::Text(disc_id.clone()));
                }
                match object {
                    Object::Track => {
                        *join |= Join::ARTIST_TRACK;
                        sql.push(self.sqlfilter("track_artists.artist_id", disc_ids, "OR"));
                    }
                    _ => (),
                }
            }
        }
        (sql, params)
    }

    fn sqlfilter(&self, column: &str, ids: &Vec<String>, operator: &str) -> String {
        if ids.len() == 0 {
            String::new()
        } else {
            let mut filters = Vec::new();
            for _id in &*ids {
                filters.push(format!(" {}=? ", column));
            }
            format!("({})", filters.join(operator))
        }
    }
}
