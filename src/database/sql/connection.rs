// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Sql thread safe Connection and Transaction

use crate::bridge::Bridge;
use crate::database::collection::Collection;
use crate::database::sql::utils::get_connection;
use crate::{to_option, to_value};

use rusqlite::ToSql;
use std::sync::{Arc, Mutex, MutexGuard, RwLock, RwLockReadGuard, RwLockWriteGuard};

pub enum RwLockGuard<'a> {
    Read(RwLockReadGuard<'a, Collection>),
    Write(RwLockWriteGuard<'a, Collection>),
    None,
}

pub enum Cursor<'a> {
    ConnectionOwned((&'a rusqlite::Connection, RwLockGuard<'a>)),
    ConnectionBorrowed((Arc<Mutex<rusqlite::Connection>>, RwLockGuard<'a>)),
    Transaction((&'a rusqlite::Transaction<'a>, RwLockGuard<'a>)),
}

pub enum RusqliteConnection {
    Owned(rusqlite::Connection),
    Borrowed(Arc<Mutex<rusqlite::Connection>>),
}

pub struct Connection<'a> {
    collection: Arc<RwLock<Collection>>,
    connection: RusqliteConnection,
    pub namespace: Option<&'a str>,
}

pub struct Transaction<'a> {
    transaction: rusqlite::Transaction<'a>,
    pub namespace: Option<&'a str>,
    _lock_guard: RwLockGuard<'a>,
}

// Reminder: Sql trait to apply to references, only owned values
// https://codeandbitters.com/static-trait-bound/
pub trait Sql<'a> {
    // Get Sql namespace
    fn namespace(&self) -> &Option<&'a str>;
    // Get current cursor
    fn cursor(&self) -> Cursor;
    // Execute query
    fn execute(&self, sql: &str, params: &[&dyn ToSql]) -> Result<i64, String>;
}

pub trait SqlValues {
    /// Get value at row 1 column 1 for query as string
    fn string(&self, query: &String, params: &[&dyn ToSql]) -> String;
    /// Get value at row 1 column 1 for query as optional string
    fn string_option(&self, query: &String, params: &[&dyn ToSql]) -> Option<String>;
    /// Get value at row 1 column 1 for query as integer
    fn integer(&self, query: &String, params: &[&dyn ToSql]) -> i64;
    /// Get value at row 1 column 1 for query as optional integer
    fn integer_option(&self, query: &String, params: &[&dyn ToSql]) -> Option<i64>;
    /// Get value at row 1 column 1 for query as float
    fn real(&self, query: &String, params: &[&dyn ToSql]) -> f64;
    /// Get value at row 1 column 1 for query as optional float
    fn real_option(&self, query: &String, params: &[&dyn ToSql]) -> Option<f64>;
    /// Get values column 1 for query as strings
    fn strings(&self, query: &String, params: &[&dyn ToSql]) -> Vec<String>;
    /// Get values column 1 for query as integers
    fn integers(&self, query: &String, params: &[&dyn ToSql]) -> Vec<i64>;
    /// Get values column 1 for query as floats
    fn reals(&self, query: &String, params: &[&dyn ToSql]) -> Vec<f64>;
    /// Get row for query
    fn row(&self, query: &String, params: &[&dyn ToSql]) -> Vec<rusqlite::types::Value>;
    /// Get rows for query
    fn rows(&self, query: &String, params: &[&dyn ToSql]) -> Vec<Vec<rusqlite::types::Value>>;
}

impl<'a> Connection<'a> {
    /// Get a new connection (read only) for all namespaces
    pub fn new(
        collection: Arc<RwLock<Collection>>,
        connection: Arc<Mutex<rusqlite::Connection>>,
    ) -> Connection<'a> {
        Connection {
            namespace: None,
            collection,
            connection: RusqliteConnection::Borrowed(connection),
        }
    }
    /// Get a new cursor for Bridge
    pub fn new_for_bridge(bridge: &Arc<Bridge>) -> Connection<'a> {
        let collection = Arc::clone(&bridge.collection);
        let connection = Arc::clone(&bridge.connection);
        Self::new(collection, connection)
    }

    // Get a new cursor for namespace
    pub fn new_for_namespace(
        collection: Arc<RwLock<Collection>>,
        namespace: &'a str,
    ) -> Connection {
        Connection {
            namespace: Some(namespace),
            collection,
            connection: RusqliteConnection::Owned(get_connection(namespace)),
        }
    }
}

impl<'a> Transaction<'a> {
    pub fn new(
        namespace: &'static str,
        mut transaction: rusqlite::Transaction<'a>,
        lock_guard: RwLockWriteGuard<'a, Collection>,
    ) -> Transaction<'a> {
        transaction.set_drop_behavior(rusqlite::DropBehavior::Commit);
        Transaction {
            transaction,
            namespace: Some(namespace),
            _lock_guard: RwLockGuard::Write(lock_guard),
        }
    }
}

impl<'a> dyn Sql<'_> + 'a {
    // Get rows for query with cursor
    pub fn getrow(
        &self,
        cursor: Cursor,
        query: &String,
        params: &[&dyn ToSql],
    ) -> Vec<rusqlite::types::Value> {
        match cursor {
            Cursor::ConnectionOwned(connection_opt) => {
                self.getrow_for_query(&connection_opt.0, query, params)
            }
            Cursor::ConnectionBorrowed(connection_opt) => {
                self.getrow_for_query(&connection_opt.0.lock().unwrap(), query, params)
            }
            Cursor::Transaction(transaction) => self.getrow_for_query(transaction.0, query, params),
        }
    }

    // Get rows for query with cursor
    pub fn getrows(
        &self,
        cursor: Cursor,
        query: &String,
        params: &[&dyn ToSql],
    ) -> Vec<Vec<rusqlite::types::Value>> {
        match cursor {
            Cursor::ConnectionOwned(connection_opt) => {
                self.getrows_for_query(&connection_opt.0, query, params)
            }
            Cursor::ConnectionBorrowed(connection_opt) => {
                self.getrows_for_query(&connection_opt.0.lock().unwrap(), query, params)
            }
            Cursor::Transaction(transaction) => {
                self.getrows_for_query(transaction.0, query, params)
            }
        }
    }

    // Get rows for query on rusqlite connection
    fn getrow_for_query(
        &self,
        connection: &rusqlite::Connection,
        query: &String,
        params: &[&dyn ToSql],
    ) -> Vec<rusqlite::types::Value> {
        let mut values = Vec::new();
        match connection.prepare(query) {
            Ok(mut stmt) => {
                let column_count = stmt.column_count();
                match stmt.query(params) {
                    Ok(mut rows) => {
                        if let Ok(option) = rows.next() {
                            if let Some(row) = option {
                                for i in 0..column_count {
                                    match row.get_ref_unwrap(i) {
                                        rusqlite::types::ValueRef::Null => {
                                            values.push(rusqlite::types::Value::Null)
                                        }
                                        rusqlite::types::ValueRef::Integer(int) => {
                                            values.push(rusqlite::types::Value::Integer(int))
                                        }
                                        rusqlite::types::ValueRef::Real(real) => {
                                            values.push(rusqlite::types::Value::Real(real))
                                        }
                                        rusqlite::types::ValueRef::Text(utf8) => {
                                            match String::from_utf8_lossy(utf8) {
                                                std::borrow::Cow::Borrowed(value) => {
                                                    values.push(rusqlite::types::Value::Text(
                                                        String::from(value),
                                                    ));
                                                }
                                                std::borrow::Cow::Owned(value) => {
                                                    values.push(rusqlite::types::Value::Text(
                                                        value.to_owned().to_string(),
                                                    ));
                                                }
                                            }
                                        }
                                        rusqlite::types::ValueRef::Blob(_blob) => {
                                            log::error!("Not implemented")
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Err(error) => {
                        log::error!("{}", error);
                    }
                }
            }
            Err(error) => {
                log::error!("{} -> {}", query, error);
            }
        }
        values
    }

    // Get rows for query on rusqlite connection
    fn getrows_for_query(
        &self,
        connection: &rusqlite::Connection,
        query: &String,
        params: &[&dyn ToSql],
    ) -> Vec<Vec<rusqlite::types::Value>> {
        let mut items = Vec::new();
        match connection.prepare(query) {
            Ok(mut stmt) => {
                let column_count = stmt.column_count();
                match stmt.query(params) {
                    Ok(mut rows) => {
                        while let Ok(Some(row)) = rows.next() {
                            let mut values = Vec::new();
                            for i in 0..column_count {
                                match row.get_ref_unwrap(i) {
                                    rusqlite::types::ValueRef::Null => {
                                        values.push(rusqlite::types::Value::Null);
                                    }
                                    rusqlite::types::ValueRef::Integer(int) => {
                                        values.push(rusqlite::types::Value::Integer(int));
                                    }
                                    rusqlite::types::ValueRef::Real(real) => {
                                        values.push(rusqlite::types::Value::Real(real));
                                    }
                                    rusqlite::types::ValueRef::Text(utf8) => {
                                        match String::from_utf8_lossy(utf8) {
                                            std::borrow::Cow::Borrowed(value) => {
                                                values.push(rusqlite::types::Value::Text(
                                                    String::from(value),
                                                ));
                                            }
                                            std::borrow::Cow::Owned(value) => {
                                                values.push(rusqlite::types::Value::Text(
                                                    value.to_owned().to_string(),
                                                ));
                                            }
                                        }
                                    }
                                    rusqlite::types::ValueRef::Blob(_blob) => {
                                        log::error!("Not implemented")
                                    }
                                }
                            }
                            items.push(values);
                        }
                    }
                    Err(error) => {
                        log::error!("{}", error);
                    }
                }
            }
            Err(error) => {
                log::error!("{} -> {}", query, error);
            }
        }
        items
    }
}

impl<'a> Sql<'a> for Connection<'a> {
    fn cursor(&self) -> Cursor {
        match &self.connection {
            RusqliteConnection::Owned(connection) => Cursor::ConnectionOwned((
                connection,
                RwLockGuard::Write(self.collection.write().unwrap()),
            )),
            RusqliteConnection::Borrowed(connection) => Cursor::ConnectionBorrowed((
                Arc::clone(connection),
                RwLockGuard::Read(self.collection.read().unwrap()),
            )),
        }
    }
    fn namespace(&self) -> &Option<&'a str> {
        &self.namespace
    }

    fn execute(&self, sql: &str, params: &[&dyn ToSql]) -> Result<i64, String> {
        let mut connection: Option<&rusqlite::Connection> = None;
        let arc_connection: Arc<Mutex<rusqlite::Connection>>;
        let mutex_guard: MutexGuard<rusqlite::Connection>;
        match self.cursor() {
            Cursor::ConnectionOwned(cursor) => {
                connection = Some(cursor.0);
            }
            Cursor::ConnectionBorrowed(cursor) => {
                arc_connection = Arc::clone(&cursor.0);
                mutex_guard = arc_connection.lock().unwrap();
                connection = Some(&mutex_guard);
            }
            _ => {}
        }
        match connection {
            Some(cnx) => cnx
                .execute(sql, params)
                .map(|_i| -> i64 { cnx.last_insert_rowid() })
                .map_err(|e| -> String { format!("{}: {}", e, sql) }),
            None => Err(String::from("No connection available")),
        }
    }
}

impl<'a> Sql<'a> for Transaction<'a> {
    fn cursor(&self) -> Cursor {
        Cursor::Transaction((&self.transaction, RwLockGuard::None))
    }
    fn namespace(&self) -> &Option<&'a str> {
        &self.namespace
    }
    fn execute(&self, sql: &str, params: &[&dyn ToSql]) -> Result<i64, String> {
        self.transaction
            .execute(sql, params)
            .map(|_i| -> i64 { self.transaction.last_insert_rowid() })
            .map_err(|e| -> String { format!("{}: {}", e, sql) })
    }
}

impl<'a> SqlValues for dyn Sql<'_> + 'a {
    fn string(&self, query: &String, params: &[&dyn ToSql]) -> String {
        let columns = self.getrow(self.cursor(), query, params);
        if columns.len() > 0 {
            return to_value!(&columns[0], str);
        }
        String::new()
    }
    fn string_option(&self, query: &String, params: &[&dyn ToSql]) -> Option<String> {
        let columns = self.getrow(self.cursor(), query, params);
        if columns.len() > 0 {
            return to_option!(&columns[0], str);
        }
        None
    }
    fn integer(&self, query: &String, params: &[&dyn ToSql]) -> i64 {
        let columns = self.getrow(self.cursor(), query, params);
        if columns.len() > 0 {
            return to_value!(&columns[0], i64);
        }
        0
    }
    fn integer_option(&self, query: &String, params: &[&dyn ToSql]) -> Option<i64> {
        let columns = self.getrow(self.cursor(), query, params);
        if columns.len() > 0 {
            return to_option!(&columns[0], i64);
        }
        None
    }
    fn real(&self, query: &String, params: &[&dyn ToSql]) -> f64 {
        let columns = self.getrow(self.cursor(), query, params);
        if columns.len() > 0 {
            return to_value!(&columns[0], f64);
        }
        0.0
    }
    fn real_option(&self, query: &String, params: &[&dyn ToSql]) -> Option<f64> {
        let columns = self.getrow(self.cursor(), query, params);
        if columns.len() > 0 {
            return to_option!(&columns[0], f64);
        }
        None
    }
    fn strings(&self, query: &String, params: &[&dyn ToSql]) -> Vec<String> {
        let mut strings = Vec::new();
        for result in self.getrows(self.cursor(), query, params) {
            for column in result {
                if let rusqlite::types::Value::Text(string) = column {
                    strings.push(string);
                    break;
                }
            }
        }
        strings
    }
    fn integers(&self, query: &String, params: &[&dyn ToSql]) -> Vec<i64> {
        let mut integers = Vec::new();
        for result in self.getrows(self.cursor(), query, params) {
            for column in result {
                if let rusqlite::types::Value::Integer(integer) = column {
                    integers.push(integer);
                    break;
                }
            }
        }
        integers
    }
    fn reals(&self, query: &String, params: &[&dyn ToSql]) -> Vec<f64> {
        let mut reals = Vec::new();
        for result in self.getrows(self.cursor(), query, params) {
            for column in result {
                if let rusqlite::types::Value::Real(real) = column {
                    reals.push(real);
                    break;
                }
            }
        }
        reals
    }
    fn row(&self, query: &String, params: &[&dyn ToSql]) -> Vec<rusqlite::types::Value> {
        self.getrow(self.cursor(), query, params)
    }
    fn rows(&self, query: &String, params: &[&dyn ToSql]) -> Vec<Vec<rusqlite::types::Value>> {
        self.getrows(self.cursor(), query, params)
    }
}
