// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! SQL Macros

// Convert connection/transaction to sql
#[macro_export]
macro_rules! to_sql {
    ($item: expr) => {
        &$item as &dyn Sql
    };
}

#[macro_export]
macro_rules! transaction {
    ($collection: expr, $connection: expr, $namespace: expr) => {
        Transaction {
            namespace: Some($namespace),
            collection: $collection,
            transaction: $connection.transaction().ok().unwrap(),
        }
    };
}

#[macro_export]
macro_rules! params_from_values {
    () => {
        Vec<&dyn rusqlite::ToSql>::new()
    };
    ($values:expr) => {{
        let mut params: Vec<&dyn rusqlite::ToSql> = Vec::new();
        for value in $values.iter() {
            params.push(value as &dyn rusqlite::ToSql);
        }
        params
    }};
}

#[macro_export]
macro_rules! to_value {
    ($x:expr, bool) => {
        match $x {
            rusqlite::types::Value::Integer(value) => *value != 0,
            _ => false,
        }
    };
    ($x:expr, i64) => {
        match $x {
            &rusqlite::types::Value::Integer(value) => value,
            _ => 0,
        }
    };
    ($x:expr, f64) => {
        match $x {
            &rusqlite::types::Value::Real(value) => value,
            _ => 0.0,
        }
    };
    ( $x:expr, str) => {
        match $x {
            rusqlite::types::Value::Text(value) => value.to_string(),
            _ => String::new(),
        }
    };
}

#[macro_export]
macro_rules! to_option {
    ($x:expr, i64) => {
        match $x {
            &rusqlite::types::Value::Integer(value) => Some(value),
            _ => None,
        }
    };
    ($x:expr, f64) => {
        match $x {
            &rusqlite::types::Value::Real(value) => Some(value),
            _ => None,
        }
    };
    ($x:expr, str) => {
        match $x {
            rusqlite::types::Value::Text(value) => Some(value.to_string()),
            _ => None,
        }
    };
}
