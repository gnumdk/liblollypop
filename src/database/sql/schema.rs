// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

pub static SCHEMA: [&'static str; 31] = [
    "CREATE TABLE albums (
        id TEXT NOT NULL,
        title TEXT NOT NULL,
        mbid TEXT,
        is_compilation BOOLEAN NOT NULL,
        uri TEXT NOT NULL,
        popularity INT NOT NULL,
        timestamp INT,
        year INT,
        rate INT NOT NULL,
        status INT NOT NULL,
        synced INT NOT NULL)",
    "CREATE TABLE discs (
        id TEXT NOT NULL,
        album_id TEXT NOT NULL,
        number INT NOT NULL,
        title TEXT)",
    "CREATE TABLE artists (
        id TEXT NOT NULL,
        name TEXT NOT NULL,
        sortname TEXT NOT NULL,
        mbid TEXT)",
    "CREATE TABLE genres (
        id TEXT NOT NULL,
        name TEXT NOT NULL)",
    "CREATE TABLE album_artists (
        album_id TEXT NOT NULL,
        artist_id TEXT NOT NULL)",
    "CREATE TABLE disc_artists (
        disc_id TEXT NOT NULL,
        artist_id TEXT NOT NULL)",
    "CREATE TABLE disc_genres (
        disc_id TEXT NOT NULL,
        genre_id TEXT NOT NULL)",
    "CREATE TABLE albums_timed_popularity (
        album_id TEXT NOT NULL,
        mtime INT NOT NULL,
        popularity INT NOT NULL)",
    "CREATE TABLE tracks (
        id TEXT NOT NULL,
        title TEXT NOT NULL,
        uri TEXT NOT NULL,
        duration INT,
        number INT,
        disc_id TEXT NOT NULL,
        timestamp INT,
        year INT,
        popularity INT NOT NULL,
        status INT NOT NULL DEFAULT 0,
        rate INT NOT NULL,
        ltime INT NOT NULL,
        mtime INT NOT NULL,
        mbid TEXT,
        bpm DOUBLE)",
    "CREATE TABLE featuring (
        artist_id TEXT NOT NULL,
        album_id TEXT NOT NULL)",
    "CREATE TABLE track_genres (
        track_id TEXT NOT NULL,
        genre_id TEXT NOT NULL)",
    "CREATE TABLE track_artists (
        track_id TEXT NOT NULL,
        artist_id TEXT NOT NULL)",
    "CREATE TABLE track_album_artists (
        track_id TEXT NOT NULL,
        artist_id TEXT NOT NULL)",
    "CREATE TABLE track_performers (
        track_id TEXT NOT NULL,
        performer_id TEXT NOT NULL)",
    "CREATE TABLE track_remixers (
        track_id TEXT NOT NULL,
        remixer_id TEXT NOT NULL)",
    "CREATE TABLE track_conductors (
        track_id TEXT NOT NULL,
        conductor_id TEXT NOT NULL)",
    "CREATE TABLE track_composers (
        track_id TEXT NOT NULL,
        composer_id TEXT NOT NULL)",
    "CREATE UNIQUE index idx_a1 ON artists(id)",
    "CREATE UNIQUE index idx_a2 ON albums(id)",
    "CREATE UNIQUE index idx_g ON genres(id)",
    "CREATE UNIQUE index idx_d ON discs(id)",
    "CREATE UNIQUE index idx_t ON tracks(id)",
    "CREATE UNIQUE index idx_aa ON album_artists(album_id, artist_id)",
    "CREATE UNIQUE index idx_da ON disc_artists(disc_id, artist_id)",
    "CREATE UNIQUE index idx_ta ON track_artists(track_id, artist_id)",
    "CREATE UNIQUE index idx_dg ON disc_genres(disc_id, genre_id)",
    "CREATE UNIQUE index idx_tg ON track_genres(track_id, genre_id)",
    "CREATE UNIQUE index idx_tp ON track_performers(track_id, performer_id)",
    "CREATE UNIQUE index idx_tr ON track_remixers(track_id, remixer_id)",
    "CREATE UNIQUE index idx_tc1 ON track_conductors(track_id, conductor_id)",
    "CREATE UNIQUE index idx_tc2 ON track_composers(track_id, composer_id)",
];

pub static UPGRADE: [&'static str; 0] = [];
