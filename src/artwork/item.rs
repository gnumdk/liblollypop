// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use crate::ArtworkSize;
use crate::ModelType;

#[derive(Clone)]
pub struct ArtworkItem {
    pub id: String,
    pub type_: ModelType,
    pub size: ArtworkSize,
}

impl ArtworkItem {
    /// Constructs a new ArtworkItem
    /// # Parameters
    ///
    /// * `id`: Item id in database
    /// * `type_`: Item type (Artist, Album, Disc, Track)
    /// * `size`: Wanted artwork size
    pub fn new(id: String, type_: ModelType, size: ArtworkSize) -> Self {
        Self { id, type_, size }
    }
}
