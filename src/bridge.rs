// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use crate::database::collection::Collection;
use crate::database::sql::utils::get_ro_connection;
use crate::Signal;

use gtk::glib::Sender;

use std::sync::{Arc, Mutex, RwLock};

/// Bridge between plugins and Lollypop
pub struct Bridge {
    /// Needed to get a lock on database
    pub collection: Arc<RwLock<Collection>>,
    /// Read only Connection to database
    pub connection: Arc<Mutex<rusqlite::Connection>>,
    /// Send message to Lollypop
    pub sender: Arc<RwLock<Sender<Signal>>>,
}

impl Bridge {
    #[doc(hidden)]
    pub fn new(sender: Arc<RwLock<Sender<Signal>>>) -> Bridge {
        Bridge {
            collection: Arc::new(RwLock::new(Collection::new())),
            connection: Arc::new(Mutex::new(get_ro_connection(&vec![]))),
            sender,
        }
    }
}
