// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use crate::utils::bytes::{as_u32_le, fix_utf16, remove_utf16_sep, split};
use encoding_rs;
use log::error;
use std::convert::TryInto;
use std::str;

const LATIN1_ENCODING: &u8 = &b'\x00';
const UTF_16_ENCODING: &u8 = &b'\x01';
const UTF_16BE_ENCODING: &u8 = &b'\x02';
const UTF_8_ENCODING: &u8 = &b'\x03';

const LOG: &str = "lib::tags::tagframe";

#[derive(Clone)]
/// Synced lyrics with timestamp and related sentence
pub struct SyncedLyrics {
    pub timestamp: u32,
    pub lyrics: String,
}

// Frame with encoded information
#[doc(hidden)]
pub struct TagFrame<'a> {
    bytes: &'a [u8],
}

impl<'a> TagFrame<'a> {
    // Get a new tag frame
    pub fn new(bytes: &'a [u8]) -> Self {
        Self { bytes: bytes }
    }

    // Get tag frame as String
    pub fn text(&self) -> String {
        let encoding = self.encoding();
        match &encoding {
            LATIN1_ENCODING | UTF_8_ENCODING => {
                let parts = split(self.body(), &[0]);
                if let Some(last) = parts.last() {
                    let part = last.to_vec();
                    if &encoding == LATIN1_ENCODING {
                        let mut utf8 = vec![0; part.len() * 2];
                        let count =
                            encoding_rs::mem::convert_latin1_to_utf8(&part, utf8.as_mut_slice());
                        return String::from_utf8_lossy(&utf8.as_slice()[..count]).into_owned();
                    } else {
                        return String::from_utf8_lossy(&part).into_owned();
                    }
                }
            }
            UTF_16_ENCODING | UTF_16BE_ENCODING => {
                let parts: Vec<&[u8]>;
                if &encoding == UTF_16_ENCODING {
                    parts = split(self.body(), &[255, 254]);
                } else {
                    parts = split(self.body(), &[0, 0]);
                }
                if let Some(last) = parts.last() {
                    let mut part = last.to_vec();
                    fix_utf16(&mut part);
                    let utf16: Vec<u16> = part
                        .chunks_exact(2)
                        .into_iter()
                        .map(|a| u16::from_be_bytes([a[0], a[1]]))
                        .collect();
                    return String::from_utf16_lossy(&utf16.as_slice());
                }
            }
            _ => {}
        }
        String::new()
    }

    // Get tag frame as integer
    pub fn integer(&self) -> u8 {
        return self.bytes[11];
    }

    // Get synced lyrics from tag frame
    pub fn synced_lyrics(&self) -> Vec<SyncedLyrics> {
        let mut synced_lyrics: Vec<SyncedLyrics> = Vec::new();
        let mut lines: Vec<&[u8]> = Vec::new();
        let encoding = self.encoding();
        match &encoding {
            LATIN1_ENCODING | UTF_8_ENCODING => {
                lines = split(self.body(), &[10]);
            }
            UTF_16_ENCODING => {
                lines = split(self.body(), &[10, 0]);
            }
            UTF_16BE_ENCODING => {
                lines = split(self.body(), &[0, 10]);
            }
            _ => {}
        }
        match &encoding {
            LATIN1_ENCODING | UTF_8_ENCODING => {
                let mut index = 1;
                while index < lines.len() {
                    let line = lines[index];
                    let synced: SyncedLyrics;
                    let timestamp: u32;
                    let length = line.len();
                    if length < 5 {
                        continue;
                    }
                    match line[length - 5..length - 1].try_into() {
                        Ok(value) => {
                            timestamp = as_u32_le(value);
                        }
                        Err(error) => {
                            error!("{}::synced_lyrics: {}", LOG, error);
                            continue;
                        }
                    }
                    let lyrics = &line[0..line.len() - 5];
                    if &encoding == LATIN1_ENCODING {
                        let mut utf8 = vec![0; lyrics.len() * 2];
                        let count =
                            encoding_rs::mem::convert_latin1_to_utf8(&lyrics, utf8.as_mut_slice());
                        synced = SyncedLyrics {
                            timestamp: timestamp,
                            lyrics: String::from_utf8_lossy(&utf8.as_slice()[..count]).into_owned(),
                        };
                    } else {
                        synced = SyncedLyrics {
                            timestamp: timestamp,
                            lyrics: String::from_utf8_lossy(&lyrics).into_owned(),
                        };
                    }
                    synced_lyrics.push(synced);
                    index += 1;
                }
            }
            UTF_16_ENCODING | UTF_16BE_ENCODING => {
                let mut index = 1;
                while index < lines.len() {
                    let line = remove_utf16_sep(lines[index]);
                    let timestamp: u32;
                    let length = line.len();
                    if length < 5 {
                        continue;
                    }
                    match line[length - 5..length - 1].try_into() {
                        Ok(value) => {
                            timestamp = as_u32_le(value);
                        }
                        Err(error) => {
                            error!("{}::synced_lyrics: {}", LOG, error);
                            continue;
                        }
                    }
                    let mut lyrics = Vec::from(&line[0..line.len() - 6]);
                    fix_utf16(&mut lyrics);
                    let utf16: Vec<u16> = lyrics
                        .chunks_exact(2)
                        .into_iter()
                        .map(|a| u16::from_be_bytes([a[0], a[1]]))
                        .collect();
                    let synced = SyncedLyrics {
                        timestamp: timestamp,
                        lyrics: String::from_utf16_lossy(&utf16.as_slice()),
                    };
                    synced_lyrics.push(synced);
                    index += 1;
                }
            }
            _ => {}
        }

        synced_lyrics
    }

    // Get frame body
    pub fn body(&self) -> &[u8] {
        &self.bytes[11..]
    }

    // Get frame key
    pub fn key(&self) -> String {
        match str::from_utf8(&self.bytes[0..4]) {
            Ok(value) => {
                return String::from(value);
            }
            Err(_error) => {
                return String::new();
            }
        }
    }

    // Get frame encoding
    pub fn encoding(&self) -> u8 {
        self.bytes[10]
    }
}
