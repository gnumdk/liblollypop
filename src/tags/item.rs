// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Collection items usable with liblollypop::database::methods::*::add_item()

use crate::database::objects::properties::Property;

use std::collections::HashMap;

/// Represent an Artist, an Album, a Genre
#[derive(Debug)]
pub struct Item {
    pub properties: HashMap<Property, rusqlite::types::Value>,
}

/// Represent a disc with its relations
#[derive(Debug)]
pub struct DiscItem {
    pub properties: HashMap<Property, rusqlite::types::Value>,
    pub album_item: Item,
    pub genre_items: Vec<Item>,
    pub artist_items: Vec<Item>,
}

/// Represent a track with its relations
#[derive(Debug)]
pub struct TrackItem {
    pub properties: HashMap<Property, rusqlite::types::Value>,
    pub disc_item: DiscItem,
    pub artist_items: Vec<Item>,
    pub performer_items: Vec<Item>,
    pub conductor_items: Vec<Item>,
    pub remixer_items: Vec<Item>,
    pub composer_items: Vec<Item>,
}
