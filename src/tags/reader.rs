// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Helper to read tags from files with GStreamer
//!
//! Example:
//! ```no_run
//! use liblollypop::tags::reader::TagsReader;
//! use liblollypop::tags::reader::{TagKey, TagValue};
//! let mut tagreader = TagsReader::new();
//! let mtime = 126545646510;
//! let file = tagreader.file_for_uri("file:///file.mp3", mtime).unwrap();
//! let track_number = liblollypop::cast_enum!(
//!    file.get(&TagKey::TrackNumber),
//!    TagValue::TrackNumber
//! );
//! let title = liblollypop::cast_enum!(file.get(&TagKey::AlbumTitle), TagValue::AlbumTitle);
//!```

use super::frame::{SyncedLyrics, TagFrame};
use crate::utils::date::iso_date_from_string;

use gettextrs::*;
use gstreamer::glib::value::FromValue;
use gstreamer::{tags, ClockTime};
use gstreamer_pbutils::Discoverer;

use log::error;
use regex::Regex;
use std::convert::TryFrom;
use strum_macros::EnumIter;

/// Supported tag keys
#[derive(Debug, EnumIter, PartialEq, Eq, Hash)]
pub enum TagKey {
    Genres,
    AlbumTitle,
    Title,
    DiscTitle,
    Artists,
    Composers,
    Performers,
    Remixers,
    Conductors,
    AlbumArtists,
    AlbumMbid,
    ArtistMbids,
    AlbumArtistMbids,
    TrackMbid,
    Version,
    Bpm,
    TrackNumber,
    DiscNumber,
    IsCompilation,
    Date,
    OriginalDate,
    Rate,
    ArtistSortnames,
    AlbumArtistSortnames,
    Duration,
}

/// Supported tag values
#[derive(Debug)]
pub enum TagValue {
    Genres(Vec<String>),
    AlbumTitle(String),
    Title(String),
    DiscTitle(Option<String>),
    Artists(Vec<String>),
    Composers(Vec<String>),
    Performers(Vec<String>),
    Remixers(Vec<String>),
    Conductors(Vec<String>),
    AlbumArtists(Vec<String>),
    AlbumMbid(Option<String>),
    ArtistMbids(Vec<String>),
    AlbumArtistMbids(Vec<String>),
    TrackMbid(Option<String>),
    Version(Option<String>),
    Bpm(Option<i64>),
    TrackNumber(Option<i64>),
    DiscNumber(Option<i64>),
    IsCompilation(bool),
    Date(Option<(i64, i64)>),
    OriginalDate(Option<(i64, i64)>),
    Rate(i64),
    ArtistSortnames(Vec<String>),
    AlbumArtistSortnames(Vec<String>),
    Duration(i64),
}

/// Read tags from files
pub struct TagsReader {
    discoverer: Discoverer,
}

// A file with tags
#[derive(Default, Debug)]
pub struct TagsFile<'a> {
    pub uri: &'a str,
    pub mtime: i64,
    pub duration: i64,
    pub tags: Option<tags::TagList>,
}

#[derive(Clone)]
enum PrivateFrame {
    Integer(u8),
    String(String),
    SyncedLyrics(SyncedLyrics),
    None,
}

impl TagsReader {
    /// Constructs a new tag reader
    pub fn new() -> Self {
        TagsReader {
            discoverer: Discoverer::new(ClockTime::from_seconds(10)).unwrap(),
        }
    }

    /// Get tags list for URI
    pub fn file_for_uri<'a>(&mut self, uri: &'a str, mtime: i64) -> Result<TagsFile<'a>, String> {
        match self.discoverer.discover_uri(uri) {
            Ok(discoverer_info) => {
                let mut duration = 0;
                if let Some(clock_time) = discoverer_info.duration() {
                    if let Ok(i64_mseconds) = i64::try_from(clock_time.mseconds()) {
                        duration = i64_mseconds;
                    }
                }
                Ok(TagsFile::new(uri, mtime, duration, discoverer_info.tags()))
            }
            Err(error) => Err(format!("{:?}: {}", error, uri)),
        }
    }
}

impl<'a> TagsFile<'a> {
    /// Construct a new tags file
    pub fn new(uri: &'a str, mtime: i64, duration: i64, tags: Option<tags::TagList>) -> Self {
        Self {
            uri,
            mtime,
            duration,
            tags,
        }
    }

    /// Get key value for tags
    pub fn get(&self, key: &TagKey) -> TagValue {
        match key {
            TagKey::Genres => TagValue::Genres(self.values("genre")),
            TagKey::AlbumTitle => {
                TagValue::AlbumTitle(self.value("album").unwrap_or(gettext("Unknown")))
            }
            TagKey::Title => {
                match self.value::<String>("title") {
                    Some(title) => TagValue::Title(title.clone()),
                    None => {
                        // Try to get from filename
                        let split = self.uri.split('/');
                        let split_vec = split.collect::<Vec<&str>>();
                        match split_vec.last() {
                            Some(value) => return TagValue::Title(String::from(*value)),
                            _ => {}
                        }
                        TagValue::Title(gettext("Unknown"))
                    }
                }
            }
            TagKey::DiscTitle => {
                for key in &["DISCSUBTITLE"] {
                    match self.extended(key) {
                        Some(title) => return TagValue::DiscTitle(Some(title)),
                        None => {}
                    }
                }
                TagValue::DiscTitle(None)
            }
            TagKey::Artists => TagValue::Artists(self.values("artist")),
            TagKey::Composers => TagValue::Composers(self.values("composer")),
            TagKey::Performers => TagValue::Performers(self.values("performer")),
            TagKey::Remixers => TagValue::Remixers(self.values("remixer")),
            TagKey::Conductors => TagValue::Conductors(self.values("conductor")),
            TagKey::AlbumArtists => TagValue::AlbumArtists(self.values("album-artist")),
            TagKey::AlbumMbid => {
                let mut ids = self.mb_id("albumid");
                if ids.len() == 0 {
                    TagValue::AlbumMbid(None)
                } else {
                    TagValue::AlbumMbid(Some(ids.remove(0)))
                }
            }
            TagKey::AlbumArtistMbids => TagValue::AlbumArtistMbids(self.mb_id("albumartistid")),
            TagKey::ArtistMbids => TagValue::ArtistMbids(self.mb_id("artistid")),
            TagKey::TrackMbid => {
                let mut ids = self.mb_id("trackid");
                if ids.len() == 0 {
                    TagValue::TrackMbid(None)
                } else {
                    TagValue::TrackMbid(Some(ids.remove(0)))
                }
            }
            TagKey::Version => TagValue::Version(self.value("version")),
            TagKey::Bpm => TagValue::Bpm(self.value("beats-per-minute")),
            TagKey::TrackNumber => {
                let mut tracknumber = self.value::<u32>("track-number");
                if tracknumber == None {
                    let re = Regex::new(r"^([0-9]*)[ ]*-").unwrap();
                    if let Some(cap) = re.captures(self.uri) {
                        tracknumber = cap
                            .get(1)
                            .map_or(Some(1), |m| m.as_str().parse::<u32>().ok());
                    }
                }
                TagValue::TrackNumber(
                    tracknumber
                        .map(|v| i32::try_from(v).ok().unwrap_or(0))
                        .map(|v| i64::try_from(v).ok().unwrap_or(0)),
                )
            }
            TagKey::DiscNumber => {
                let discnumber = self.value::<u32>("album-disc-number");
                TagValue::DiscNumber(
                    discnumber
                        .map(|v| i32::try_from(v).ok().unwrap_or(0))
                        .map(|v| i64::try_from(v).ok().unwrap_or(0)),
                )
            }
            TagKey::IsCompilation => {
                //FIXME Not handling OGG tags
                match self.private("TCMP") {
                    PrivateFrame::String(compilation) => {
                        TagValue::IsCompilation(compilation == "1")
                    }
                    _ => TagValue::IsCompilation(false),
                }
            }
            TagKey::Date => {
                let year: i32;
                let month: i32;
                let day: i32;
                match self.value::<gstreamer::DateTime>("datetime") {
                    Some(datetime) => {
                        year = datetime.year();
                        match datetime.month() {
                            Some(value) => {
                                month = value;
                            }
                            None => {
                                month = 1;
                            }
                        }
                        match datetime.day() {
                            Some(value) => {
                                day = value;
                            }
                            None => {
                                day = 1;
                            }
                        }
                        match gstreamer::DateTime::from_local_time(
                            year,
                            Some(month),
                            Some(day),
                            Some(0),
                            Some(0),
                            Some(0.0),
                        ) {
                            Ok(datetime) => match datetime.to_g_date_time() {
                                Ok(g_datetime) => {
                                    return TagValue::Date(Some((
                                        i64::from(datetime.year()),
                                        g_datetime.to_unix(),
                                    )));
                                }
                                Err(error) => {
                                    error!("{}", error);
                                }
                            },
                            Err(error) => {
                                error!("{}", error);
                            }
                        }
                    }
                    None => {}
                }
                TagValue::Date(None)
            }
            TagKey::OriginalDate => {
                let date: Option<String>;
                match self.private("TDOR") {
                    PrivateFrame::String(value) => {
                        date = Some(value);
                    }
                    _ => {
                        date = self.extended("ORIGINALDATE");
                    }
                }
                if let Some(value) = date {
                    let date = iso_date_from_string(value.as_str());
                    match gtk::glib::DateTime::from_iso8601(date.as_str(), None) {
                        Ok(datetime) => {
                            return TagValue::OriginalDate(Some((
                                i64::from(datetime.year()),
                                datetime.to_unix(),
                            )));
                        }
                        Err(_error) => {}
                    }
                }
                TagValue::OriginalDate(None)
            }
            TagKey::Rate => {
                let mut popularity = 0;
                match self.private("POPM") {
                    PrivateFrame::Integer(popm) => match popm {
                        tmp if tmp >= 1 && tmp < 64 => {
                            popularity = 1;
                        }
                        tmp if tmp >= 64 && tmp < 128 => {
                            popularity = 2;
                        }
                        tmp if tmp >= 128 && tmp < 196 => {
                            popularity = 3;
                        }
                        tmp if tmp >= 196 && tmp < 255 => {
                            popularity = 4;
                        }
                        _ => {
                            popularity = 5;
                        }
                    },
                    _ => {}
                }
                TagValue::Rate(popularity)
            }
            TagKey::ArtistSortnames => {
                let sortnames = self.values("artist-sortname");
                if sortnames.len() > 0 {
                    TagValue::ArtistSortnames(sortnames)
                } else {
                    TagValue::ArtistSortnames(self.values("artist"))
                }
            }
            TagKey::AlbumArtistSortnames => {
                let sortnames = self.values("album-artist-sortname");
                if sortnames.len() > 0 {
                    TagValue::AlbumArtistSortnames(sortnames)
                } else {
                    TagValue::AlbumArtistSortnames(self.values("album-artist"))
                }
            }
            TagKey::Duration => TagValue::Duration(self.duration),
        }
    }

    /// Get lyrics from tags
    pub fn lyrics(&self) -> String {
        match self.value::<String>("lyrics") {
            Some(lyrics) => {
                return lyrics;
            }
            None => {}
        }
        match self.private("USLT") {
            PrivateFrame::String(lyrics) => {
                return lyrics;
            }
            _ => {}
        }
        match self.extended("LYRICS") {
            Some(lyrics) => {
                return lyrics;
            }
            None => {}
        }
        String::new()
    }

    /// Get synchronized lyrics from tags
    pub fn synced_lyrics(&self) -> Vec<SyncedLyrics> {
        let mut lyrics = vec![];
        for frame in self.privates("SYLT") {
            match frame {
                PrivateFrame::SyncedLyrics(synced_lyrics) => {
                    lyrics.push(synced_lyrics);
                }
                _ => {}
            }
        }
        lyrics
    }

    /// Get artwork from tags
    pub fn artwork(&self) -> Option<Vec<u8>> {
        let mut bytes = self.bytes("image");
        if bytes.len() == 0 {
            bytes = self.bytes("preview-image");
        }
        if bytes.len() == 0 {
            return None;
        }
        Some(bytes[0].clone())
    }

    /// Get tag values for key from tags
    fn value<'b, T: FromValue<'b>>(&'b self, key: &str) -> Option<T> {
        if let Some(list) = &self.tags {
            for idx in 0..list.size_by_name(key) {
                if let Some(value) = list.index_generic(key, idx) {
                    return value.get::<'b, T>().ok();
                }
            }
        }
        None
    }

    /// Get tag values for key from tags
    fn values<'b, T: FromValue<'b>>(&'b self, key: &str) -> Vec<T> {
        let mut values: Vec<T> = vec![];
        if let Some(list) = &self.tags {
            for idx in 0..list.size_by_name(key) {
                if let Some(value) = list.index_generic(key, idx) {
                    match value.get::<'b, T>() {
                        Ok(value_) => {
                            values.push(value_);
                        }
                        Err(error) => {
                            error!("{}", error);
                        }
                    }
                }
            }
        }
        values
    }

    /// Get tag value for key from private tags
    fn private(&self, key: &str) -> PrivateFrame {
        let values: Vec<PrivateFrame> = self.privates(key);
        if values.len() > 0 {
            return values[0].clone();
        }
        PrivateFrame::None
    }

    /// Get tag values for key from private tags
    fn privates(&self, key: &str) -> Vec<PrivateFrame> {
        let mut values: Vec<PrivateFrame> = vec![];

        for bytes in self.bytes("private-id3v2-frame") {
            let tagframe = TagFrame::new(&bytes);
            if tagframe.key() == key {
                match key {
                    "USLT" => {
                        values.push(PrivateFrame::String(tagframe.text()));
                    }
                    "TCMP" | "TDOR" => {
                        values.push(PrivateFrame::String(tagframe.text()));
                    }
                    "POPM" => {
                        values.push(PrivateFrame::Integer(tagframe.integer()));
                    }
                    "SYLT" => {
                        for item in tagframe.synced_lyrics() {
                            values.push(PrivateFrame::SyncedLyrics(item));
                        }
                    }
                    _ => {}
                }
            }
        }
        values
    }

    /// Get tag value for key from extended tags
    fn extended(&self, key: &str) -> Option<String> {
        let extendeds = self.extendeds(key);
        if extendeds.len() > 0 {
            return Some(extendeds[0].to_string());
        }
        None
    }

    /// Get tag values for key from extended tags
    fn extendeds(&self, key: &str) -> Vec<String> {
        let mut values: Vec<String> = vec![];
        let comments: Vec<String> = self.values("extended-comment");
        for comment in comments {
            if comment.starts_with(&(String::from(key) + "=")) {
                let mut split = comment.split("=");
                split.next();
                values.push(split.collect::<String>());
            }
        }
        values
    }

    /// Get all bytes values from tags
    fn bytes(&self, key: &str) -> Vec<Vec<u8>> {
        let samples: Vec<gstreamer::sample::Sample> = self.values(key);
        let mut values: Vec<Vec<u8>> = vec![];
        for sample in samples {
            if let Some(buffer) = sample.buffer_owned() {
                if let Ok(map) = buffer.into_mapped_buffer_readable() {
                    values.push(map.as_slice().to_vec());
                }
            }
        }
        values
    }

    /// Get MusicBrainz id from tags
    fn mb_id(&self, key: &str) -> Vec<String> {
        self.values(&(String::from("musicbrainz-") + key))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use gstreamer;
    use std::env;

    #[test]
    #[allow(unused_must_use)]
    fn test_tags() {
        gstreamer::init().unwrap();
        if let Ok(path) = env::var("CARGO_MANIFEST_DIR") {
            let mut tagreader = TagsReader::new();
            let mut uri: String;
            for i in 1..5 {
                println!("test{}.mp3", i);
                uri = String::from("file://");
                uri.push_str(path.as_str());
                uri.push_str(format!("/test/test{}.mp3", i).as_str());
                let file = tagreader.file_for_uri(uri.as_str(), 0).unwrap();
                assert_eq!(
                    crate::cast_enum!(file.get(&TagKey::TrackNumber), TagValue::TrackNumber),
                    Some(1)
                );
                assert_eq!(
                    crate::cast_enum!(file.get(&TagKey::Title), TagValue::Title),
                    "Daftendirekt"
                );
                assert_eq!(
                    crate::cast_enum!(file.get(&TagKey::IsCompilation), TagValue::IsCompilation),
                    true
                );
                assert_eq!(
                    crate::cast_enum!(file.get(&TagKey::Rate), TagValue::Rate),
                    4
                );
                assert_eq!(
                    crate::cast_enum!(file.get(&TagKey::Date), TagValue::Date),
                    Some((1997, 852073200))
                );
                assert_eq!(
                    crate::cast_enum!(file.get(&TagKey::OriginalDate), TagValue::OriginalDate),
                    Some((1999, 915235200))
                );
                assert_eq!(file.lyrics(), "Vive le feu! Bérurier Noir");
                let synced_lyrics = file.synced_lyrics();
                assert_eq!(synced_lyrics[0].lyrics, "J-A-I-N ");
                assert_eq!(synced_lyrics[0].timestamp, 83886080);
            }
        }
    }
}
