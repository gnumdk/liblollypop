// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

/// Split bytes slice by sep slice
///
/// # Parameters
///
/// * `bytes`: bytes slice
/// * `sep`: sep slice
pub fn split<'a>(bytes: &'a [u8], sep: &[u8]) -> Vec<&'a [u8]> {
    let mut split: Vec<&[u8]> = vec![];
    let mut previous_index = 0;
    let bytes_len = bytes.len();
    let sep_len = sep.len();
    for index in 0..bytes_len {
        // We are matching separator
        if index + 1 + sep_len < bytes_len
            && bytes[index..index + sep_len] == *sep
            && bytes[index + 1..index + 1 + sep_len] != *sep
        {
            if index > 0 {
                split.push(&bytes[previous_index..index]);
            }
            previous_index = index + sep_len;
        }
    }
    // We add any pending value
    if previous_index != bytes_len + sep_len {
        split.push(&bytes[previous_index..bytes_len]);
    }
    split
}

/// Fix broken UTF-16 bytes
///
/// # Parameters
///
/// * `bytes`: bytes slice
pub fn fix_utf16(bytes: &mut Vec<u8>) {
    if let Some(first) = bytes.first() {
        if *first != b'\x00' {
            bytes.insert(0, 0);
        }
    }
    if bytes.len() % 2 == 0 {
        bytes.push(0);
    }
}

/// Remove UTF-16 separator from bytes
/// If you are reading this code and you know what are those bytes, send me a pull request to fix
/// this comment
///
/// # Parameters
///
/// * `bytes`: bytes slice
pub fn remove_utf16_sep(bytes: &[u8]) -> Vec<u8> {
    let lyrics: Vec<u8>;
    let length = bytes.len();
    if length > 8 {
        if &bytes[length - 2..] == &[255, 254] {
            lyrics = Vec::from(&bytes[0..length - 2]);
        } else {
            lyrics = Vec::from(bytes);
        }
    } else {
        lyrics = Vec::from(bytes);
    }
    lyrics
}

/// Get 4 bytes as integer
pub fn as_u32_le(bytes: &[u8; 4]) -> u32 {
    ((bytes[0] as u32) << 0)
        + ((bytes[1] as u32) << 8)
        + ((bytes[2] as u32) << 16)
        + ((bytes[3] as u32) << 24)
}
