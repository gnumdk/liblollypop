// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
use regex::Regex;

/// Get a date as iso 8601 format
///
/// # Parameters
///
/// * `date`: date as partial iso 8601 format
/// # Return
pub fn iso_date_from_string<'a>(date: &'a str) -> String {
    let model = ["1970", "01", "01", "00", "00", "00"];
    let regex = Regex::new(r"[-:TZ]").expect("Invalid regex");
    let mut split: Vec<&str> = regex.split(date).collect();
    let mut length = split.len();
    while length < 6 {
        split.push(model[length]);
        length += 1;
    }
    format!(
        "{}-{}-{}T{}:{}:{}Z",
        split[0], split[1], split[2], split[3], split[4], split[5]
    )
}
