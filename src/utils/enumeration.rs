// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

/// Cast an enum to the inner type
#[macro_export]
macro_rules! cast_enum {
    ($target_enum: expr, $target_path: path) => {
        if let $target_path(value) = $target_enum {
            value
        } else {
            panic!("cast macro called for an invalid path");
        }
    };
}
