// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

/// Get unix timestamp
#[macro_export]
macro_rules! timestamp {
    ($now: expr) => {
        match $now.duration_since(std::time::UNIX_EPOCH) {
            // Don't want to handle errors, if this fail, use my birthday!
            Err(_error) => 353703900,
            Ok(timestamp) => timestamp.as_secs(),
        }
    };
}

// Split strings in Vec as anothers Vec items
#[macro_export]
macro_rules! split_tags_semicolon {
    ($vec: expr) => {{
        let mut new_vec = Vec::new();
        for genre in $vec.iter() {
            for str_ in genre.split(";") {
                if str_ != "" {
                    new_vec.push(String::from(str_));
                }
            }
        }
        new_vec
    }};
}

#[macro_export]
macro_rules! unwrap_or_continue {
    ($res:expr) => {
        match $res {
            Some(val) => val,
            None => {
                continue;
            }
        }
    };
}

#[macro_export]
macro_rules! unwrap_error_or_continue {
    ($res:expr) => {
        match $res {
            Ok(val) => val,
            Err(error) => {
                log::error!("{:?}", error);
                continue;
            }
        }
    };
}

#[macro_export]
macro_rules! trim {
    ($string:expr, str) => {
        $string.trim()
    };
    ($string:expr, Option<str>) => {
        match &$string {
            Some(value) => Some(value.trim()),
            None => None,
        }
    };
}

#[macro_export]
macro_rules! vec_remove {
    ($vec: expr, $value: expr) => {
        match $vec.iter().position(|x| *x == $value) {
            Some(index) => {
                $vec.remove(index);
            }
            None => {
                log::error!("vec_remove! Failed to find item")
            }
        }
    };
}
