#![cfg_attr(feature = "dox", feature(doc_cfg))]
#![allow(clippy::needless_doctest_main)]
#![doc(
    html_logo_url = "https://gitlab.gnome.org/World/lollypop/-/raw/master/data/icons/hicolor/512x512/apps/org.gnome.Lollypop.png",
    html_favicon_url = "https://gitlab.gnome.org/GNOME/libadwaita/-/raw/main/demo/data/org.gnome.Adwaita1.Demo-symbolic.svg"
)]
//! # Liblollypop
//!
//! This library contains needed stuff to build a Lollypop plugin
//!
//! # Example
//!
//! To start, clone this example template:
//! `<https://gitlab.gnome.org/gnumdk/lollypop-plugin-template>`
//!
//! A plugin needs to implement [`PluginInterface`](crate::plugin::interface::PluginInterface)
//!
//! Available plugins type are:
//!
//! * [`ArtworkPlugin`](crate::plugin::artwork::ArtworkPlugin)
//!
//! * [`CollectionPlugin`](crate::plugin::collection::CollectionPlugin)

pub mod artwork;
pub mod bridge;
pub mod database;
pub mod plugin;
pub mod providers;
pub mod tags;
pub mod utils;

use once_cell::sync::Lazy;
use gtk::glib;

pub mod storage_path {
    use super::Lazy;
    pub static COLLECTION: Lazy<String> = Lazy::new(|| {
        let path = gtk::glib::user_data_dir();
        format!("{}/lollypop-rs/collections", path.to_string_lossy())
    });
    pub static ARTWORK_ARTISTS: Lazy<String> = Lazy::new(|| {
        let path = gtk::glib::user_data_dir();
        format!("{}/lollypop-rs/artwork/artists", path.to_string_lossy())
    });
    pub static ARTWORK_ALBUMS: Lazy<String> = Lazy::new(|| {
        let path = gtk::glib::user_data_dir();
        format!("{}/lollypop-rs/artwork/albums", path.to_string_lossy())
    });
    pub static LYRICS: Lazy<String> = Lazy::new(|| {
        let path = gtk::glib::user_data_dir();
        format!("{}/lollypop-rs/lyrics", path.to_string_lossy())
    });
    pub static CACHE_ALBUMS: Lazy<String> = Lazy::new(|| {
        let path = gtk::glib::user_cache_dir();
        format!("{}/lollypop-rs/albums", path.to_string_lossy())
    });
    pub static CACHE_ARTISTS: Lazy<String> = Lazy::new(|| {
        let path = gtk::glib::user_cache_dir();
        format!("{}/lollypop-rs/artists", path.to_string_lossy())
    });
    pub static CACHE_TRACKS: Lazy<String> = Lazy::new(|| {
        let path = gtk::glib::user_cache_dir();
        format!("{}/lollypop-rs/tracks", path.to_string_lossy())
    });
}

pub enum StatusFlags {
    NONE = 1 << 0,
    LOVED = 1 << 1,
    SKIPPED = 1 << 2,
    ALL = 1 << 0 | 1 << 1 | 1 << 2,
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum ArtworkSize {
    Small,
    Medium,
    Large,
    Monster,
}

use std::ops::Deref;
impl Deref for ArtworkSize {
    type Target = i32;
    fn deref(&self) -> &i32 {
        match self {
            ArtworkSize::Small => &50,
            ArtworkSize::Medium => &100,
            ArtworkSize::Large => &150,
            ArtworkSize::Monster => &800,
        }
    }
}

#[derive(Debug)]
pub enum Signal {
    ScannerStarted,
    ScannerFinished,
    ScannerProgress(usize, usize), // current, total
}

#[derive(glib::Enum, Debug, Copy, Clone, PartialEq, Eq, Default)]
#[enum_type(name = "ModelType")]
pub enum ModelType {
    #[default]
    Genre,
    Artist,
    Album,
    Track,
    Playlist,
}

