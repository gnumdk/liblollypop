// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Provides tracks to Lollypop

/// All methods are runned in mainloop
pub trait ScannerInterface {
    /// Start scanning music files
    fn update(&self, target_uris: Vec<String>);
    /// Stop scanning music files
    fn stop(&self);
    /// Is scanning music
    fn running(&self) -> bool;
}
