// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>
//! Provides artwork to Lollypop

use crate::artwork::item::ArtworkItem;

use gtk::{gdk, glib};

/// All methods are runned in mainloop
pub trait ArtworkInterface {
    /// Get artwork for item
    fn get(&self, artwork_item: ArtworkItem) -> Option<glib::Bytes>;
    /// Set artwork for item (example: set artwork on remote app (ampache, ...)
    fn set(&self, artwork_item: ArtworkItem, artwork: Option<gdk::gdk_pixbuf::Pixbuf>);
}
